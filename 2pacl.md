@def title = "2PACL"

# The 2PACL cooling systems at CERN

_This is a bit of a long-winded ramble about our cooling systems. The cooling principle that guides these systems is called the Two-Phase Accumulator Controlled Loop (or 2PACL, no relation to Shakur) and it is a very innovative piece of technology_

## What does CERN do?

A good way of thinking about it, from an engineering point of view, is to think of CERN as a provider of a service in the form of the LHC machine, rather like a lab might offer an electron microscope. It just so happens that the machine is a gigantic ring-type particle accelerator. 

Feynman described particle colliders in terms of wrist watches. Say you wanted to figure out how they work. One (rather contrived) way of doing this would be to smash two of them together very hard, breaking them to pieces. Then, you could examine the gears and needles as they fly out, and reverse engineer how the thing is built up. That is about as good an analogy as you're ever likely to find for what the LHC does. It crashes beams of protons (or Lead ions) together, and then clicks pictures of the events that result from the collisions. The giant cameras that click the pictures are called Detectors. Annoyingly, these detectors are made up of layers of sub-systems that are _also_ often called Detectors.

![](/assets/2pacl_1.png)
~~~
<figcaption>CMS Detector. Tiny human for scale</figcaption>
~~~

Now, let's talk about those cameras. When protons collide, the energies of their collision are high enough (the proton beams travel at nearly the speed of light) that they can recreate conditions that existed moments after the Big Bang. The shower of particles that gets produced due to these collisions is then bent under the influence of a strong magnetic field. Any particle that is electrically charged will respond to the magnet. A heavier particle (with more momentum) will get bent less than a lightweight, softie particle. This is where the Silicon tracking detectors come in. Tracker detectors are sheets of small Silicon 'chips' arranged radially around the collision region. These sheets of Silicon can keep track of _where_ a particle passes through them, and when. And if you have successive layers of Silicon, you can connect the points and find out the path that a given particle took, along with how long it took to travel that path. Physicists can then ID particles by their momentum and charge. This way, they can look for unusual/unexplained physics in these collisions and thus advance our understanding of our world.

![](/assets/2pacl_2.png)

~~~<figcaption>Pixel Detector</figcaption>~~~

And so, finally, we arrive at the need for our cooling system; These Silicon electronics suffer bravely in the radiation field that they're placed in. And it so happens that, depending on the flavour of the Silicon used, the damage to the lattice structure of the crystals is minimised at temperatures anywhere from -20°C to around -40°C. The upshot is that we need an 'active' cooling system (a system where flow is forced, as opposed to, say, letting the electronics radiate heat to surrounding air). Now, Silicon is kind of like glass, and is fragile as hell. It demands that all of it be kept at nearly the same temperature, because if you have large temperature differences between the two ends of the sensor, you can easily crack or deform it. The detectors are also really densely packed (leaving space only for the smallest of cooling tubes) and because the whole region is radioactive, the fluid must be able to withstand radiation.'

This is where our story begins. We know what we have to cool. We know we barely have any space for it. And we know we need to keep temperatures stable. The 2PACL system, which is the topic of this post, solved most of the major problems that we faced when dealing with coling of detector electronics, and, in my opinion, outshone the other methods that were implemented at CERN.

## History
The genesis of the 2PACL cooling system lie in satellite cooling. How would you cool things in space?

For a lot of us, a familiar example of a cooling system is the one used to cool our car's engine. This is a kind of "single-phase" cooling system, that is, *liquid* water transports away heat from the engine to the radiators, where it is expelled to the atmosphere. No boiling or evaporation of the water occurs (normally!), and the liquid stays liquid without ever becoming steam. In such applications, we pressurise the water, thereby increasing it's boiling point, because we explicitly do *not* want boiling. This pressurise-to-raise-boiling-point is seen in reverse on the mountains where the low pressure means water boils at lower temperatures, which means your boiled egg is undercooked.

The major advantage of such a system is simplicity. Water has a very high specific heat capacity, which means that it can absorb quite a lot of heat before a kilogram of it will undergo a temperature change of 1°C. Also, water is non-toxic, widely available (you can refill it on the side of the road), you really don't care if it leaks out, and it's cheap. These systems are well understood and are easy to implement (as witnessed in your favourite Tech Youtuber's recent video installing water cooling on their gaming rig).

![](/assets/2pacl_3.png)

~~~<figcaption>Engine Cooling System</figcaption>~~~

The disadvantage here is that the Radiators needed to cool down the water that we'd just heated up are pretty massive. Secondly, while water can absorb a lot of heat without changing temperature, over the length of a practical cooling system, it *will* change temperature (sometimes by about 10°C or so). It _is_ possible to mitigate this to a certain extent by using different refrigerants, mind.

For satellites, this is less than ideal since the big radiators required are a packaging nightmare, because space comes at a premium in space _(apologies)_. Also, as I've already mentioned, sensitive electronic equipment decidedly do NOT enjoy temperature differences between themselves and their neighbouring equipment. Yet another drawback is that the tubing that we'd need would likely be quite large, making packaging everything even more troublesome.

So, engineers in the nineties started looking for an alternative solution, and very quickly realised that two-phase fluids are a good idea. A familiar example of a *two*-phase cooling system is the air conditioner and refrigerator in your house. In such systems, you actually boil the refrigerant (these refrigerants boil at low temperatures), and then pass air over surfaces cooled by the refrigerant. This then causes the air to cool down and waft soothingly over your fevered brow. Boiling starts with a tiny amount of liquid heating up and converting to vapour. To convert more of it to vapour, we add more heat to it. The circulation of the fluid is done here by the compressor.

The thing to note here is that all of the boiling occurs at a *constant* temperature. This is great for us because we value the ability to maintain a uniform temperature over our electronics. And yet, it isn't possible to just use such a system because the compressors used here circulate vapour rather than liquid, which uses up a lot of power.

> **Sidenote**: I should point out that 'Pumps' circulate _liquid_, while 'Compressors' circulate _vapour/gas_. Also, pumps generally hate having any vapour pass through them because the vapour often 'explodes' on the walls of the pump in a phenomenon called 'cavitation' and damages the pump. Likewise, compressors despise having any liquid pass through them because liquid is incompressible, and in trying to squeeze something that won't be squeezed, the poor valves of the compressor end up damaged.

The solution that engineers came up with was to have two-phase systems, but where we pumped liquid and not vapour. This gives us the benefit of being able to maintain exact temperatures, but also makes the pumping power challenge a solvable one.

These systems derived from something called a Capillary Pumped Loop (CPL). A CPL is pretty similar to the heat pipe found in your laptop, but the difference is that the CPL involves something called the Accumulator, and that is where the magic happens.

![](/assets/2pacl_4.png)
~~~<figcaption>Capillary pumped loop</figcaption>~~~

The way a heat pipe works is that the working fluid (aka the 'refrigerant') absorbs the heat from the source (say a CPU). This causes the fluid to evaporate. Now, the vapour has much lower density (kg/m3), aka it occupies a much larger volume per unit mass of the fluid (m3/kg). Since we've evaporated dense liquid into expansive vapour, the vapour tries to make space for itself by pushing things around and travelling to the cold end. At the cold end, the laptop fans cool down the fluid, recondensing it back to vapour form, and the vapour now is fed back to the hot end using the capillary action and some wick.

You could think of an accumulator like a giant hand squeezing all of the refrigerant in the system, thereby pressurizing it. Now, if you add fine motor control to the hand, you can precisely request what pressure you want in the line. Remember, though, that I mentioned that boiling occurs at a constant pressure. For a given pressure, assuming that pressure lies between the triple point pressure and the critical pressure, a pure refrigerant (as opposed to a mixture of refrigerants) has a given boiling temperature. Water boils at 100°C, yes, but only at 1 atmosphere pressure. If we raise the pressure to, say, 10 atmosphere, suddenly, you'd have to heat water to 180°C before the first bubbles appeared. But the bottomline is that pressure is a controllable property. By 'squeezing' the whole system, we can exert higher or lower pressures.

There are two ways to do this. The first is quite literally squeezing in the manner I described. This method uses an Accumulator that contains 'bellows' which are similar to the things that move the air around in an accordion. This method, however, is not the most precise thing around, and nevertheless requires some cooling apparatus inside to accommodate reducing pressures. Such accumulators are called Pressure Controlled Accumulators.

### The Accumulator
The other method is to start with a large vessel (much larger than the volume of the loop itself), which is sized to always contain a two-phase mixture of the fluid that we are working with. This vessel is connected to the loop. If we consider water, then it would have a combination of water and steam.

Now, consider what would happen if we started heating the liquid water inside. Since we are in two-phase, it indicates that we are already evaporating, but are just in an equilibrium condition where the condensation is matching the evaporation. Therefore, by adding more heat, we will increase that evaporation, and create yet more bubbles of vapour. This vapour will rise to the top of the vessel, mixing with the existing vapour. The increased vapour amount will exert a greater pressure on the liquid below. Since liquid is incompressible (we cannot squeeze it into a smaller volume), we will insead push the liquid out into the loop. In effect, we are now squeezing the loop more, increasing the pressure of the loop.

Notice what we've achieved. By heating the accumulator, we were able to raise the pressure of the loop, and thus affect the *temperature* at which the loop boils. We've achieved temperature control!

The engineers basically took this Capillary Pumped Loop system, which pumps purely passively using the capillary action, and added a mechanical pump to it, to force the liquid around at greater speeds.

## Two-Phase Accumulator Controlled Loops
Let's finally discuss how these systems look like. I've made a simplified schematic diagram along with the pressure-enthalpy diagram of the system. The pressure enthalpy diagram has the pressure on the y-axis in a log scale, and the enthalpy (which is basically a measure of the energy of the fluid) on the x-axis. The engineers picked CO2 for the systems at CERN because it had favourable properties (I might talk about CO2 as a refrigerant another time, but suffice to say it fit well).

![](/assets/2pacl_5.png)

So we begin just at the outlet of the Condenser (point 1). The condenser is a vapour compression system (the fridge cycle we discussed earlier) that cools the refrigerant to really low temperatures (of the order of < -40°C). This supercooled fluid enters the pump at low pressures and gets pressurised. The pump here is a membrane pump. It contains a movable membrane that is 'actuated' (engineer-speak for 'moved') by a hydraulic oil. That is, the piston pushes the oil, the oil pushes the membrane and the membrane pushes the CO2.

![](/assets/2pacl_6.png)

In the process, the pump adds some heat to the CO2, and we emerge at point 2, still very cold. This cold refrigerant next passes through an internal heat exchanger where it takes heat from the return fluid which has been heated up by the detector heat. This internal heat exchanger is basically a concentric tube with the supply fluid on the inside and the return fluid on the outside, and is VERY long (~100 meters or so). Because it is so long, by the time the supply fluid comes out, it has heated up completely to the temperature of the return fluid. (point 3).

Now, the CO2 passes through an 'expansion' device, which could be just a very small valve or a capillary tube. The effect here is the same as when you pinch the outlet of a water hose; the upstream fluid is higher pressure and the downstream fluid is faster but lower pressure. Our CO2 also expands and neatly arrives at the start of our evaporator (the cooling tubes inside the detector) just about starting to boil. Thus, we've wasted no boiling performance, and are ready to start cooling (point 4).

As the CO2 moves along the pipe, it picks up more and more heat from the detector until we've reached point 5. Notice that point 4 to point 5 is along a horizontal line. This is what I mean by boiling happening at constant temperature. In the two-phase region of a P-h diagram (the area under the 'bell'/'dome'), the isothermal line is horizontal.

Now, the point 5 fluid comes back through the internal heat exchanger and is partially condensed (point 6). Still, there is more condensing to do, because we want to make sure that we don't enter the pump with any vapour remaining. This remaining condensation is done by the Condenser, and we return where we started.

Congratulations! Detector cooling achieved.

The missing piece is the pressure of points 1, 4, 5 and 6. This is where the accumulator comes in. If we start heating up the accumulator, we will raise this point upwards, and if we cool the accumulator, we will lower the pressure. This is shown in the figure below.

![](/assets/2pacl_7.png)

State A is where we were in steady state, shown as approximately -20°C in the evaporator. Now, the request comes from the detector to go warmer in temperature. The accumulator receives this request, and its PID controller immediately starts heating up the vessel. The heater raises the vapour pressure and pushes out some fluid into the loop, raising the pressure as it does. All the while, the rest of the systme is stable. The condenser is still subcooling the CO2 at the pump inlet, the internal heat exchanger is still making sure that the supply fluid is correctly heated up, and the evaporator never faces any sudden changes. 

The key point here is that, to address the 'change the temperature, please' request from the detector, *we only needed to control* one *parameter*! And herein lies the real genius of the system; its idiot-proofing. ALL of the control has been boiled down to just controlling the accumulator pressure. It doesn't matter, for instance, how much we subcool in the condenser because the internal heat exchanger will take care of it.  This robustness is a very valuable thing in engineering indeed, and this is a big part of why these systems have been so succesful.