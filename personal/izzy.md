# Izzy Stradlin

> "It's Izzy's goal to be Mr. Invisible" - Axl Rose

Sweet child o' mine is a song that Slash doesn't like very much. But it's one of Guns n' Roses' most famous songs. I first watched the video back in high school, and was influenced by just how damn cool everyone looked. One guy, in particular, stuck out from the rest. Long, messy hair, a cigarette hanging limply from his mouth, playing a white guitar.

![](/assets/izzy1.jpg)

Born Jeff Dean Isbell in the town where Purdue University is located (Lafayette, Indiana), Izzy Stradlin is the latest artist I'd like to write about. And what a life this one has lead.

## Before Guns n' Roses
Stradlin, in Lafayette, had been a drummer for a school band (his *grandmother* was also a drummer). The story famously begins with a huge commotion at his school. He turns around to see Axl flying past, a bunch of teachers giving chase. He saw the whole scene playing out and thought "here's a guy who's completely crazy, he'd be a fucking great singer". He managed to coax Axl into giving singing a try, and in the process laid the foundation for what would later become GnR.

![](/assets/izzy2.jpg)

_High school yearbook picture_

On graduating (with a D average), he moved to Los Angeles to try and make it as a musician, the reason for doing so being ever so Izzy: "The weather was better and that's where everything was".

In LA, he started out playing drums for a band called Naughty Women, cross-dressing like the other band members and playing punk music. His first gig ended with him nearly getting beaten up by skinheads who objected to "f\*ggots" wearing spandex. He got out alive defending himself by using a cymbal as a shield. At the time, he sold air-conditioning units door to door with fellow band member Bill Evans, and got booted from the band after he made an inappropriate move on Evans' girlfriend.

> "We were called the Naughty Women. At the time I thought they must have it together because they had business cards."

He stumbled into another group called The Atoms, now living out of his car. Except his car got stolen, and the drums that he used to store in it got stolen with it. So, a "fuck it" was uttered, and he bought a bass instead, joining a group called The Shire. This lasted till 1983, when he ditched the bass and switched to rhythm guitar, enjoying the fact that it was easier to write songs on. As Duff McKagan says, Izzy would always be seen with an acoustic in his hand, and bits and scraps of scribbled paper all around him. The same year, he also started a band with Axl, calling it Hollywood Rose.

Eventually, in 1985, the first lineup of Guns n' Roses was formed. And, a few short months later, the now-classic lineup was together: Axl, Slash, Izzy, Duff and Adler.

## Guns n' Roses

GnR espoused the cliche of the rockstar lifestyle perfectly. Many drinks, many drugs, many women. This image no doubt helped fuel their rise, but the style had musical substance to back it up.

The classic lineup members brought diverse influences to the table. I saw a YouTube comment that said: *"...Duff wanted to be the new New York Dolls, Slash the new Aerosmith, Izzy the new Rolling Stones and Axl the new Queen..."*. And Adler's drumming rounded it off with its relaxed swing. This plurality of tastes worked brilliantly, and their first album sounded fresh and far from formulaic. It came out in 1987 and blew everyone away.

![](/assets/izzy3.jpg)

A year later, their next album (G N' R Lies), launched to many plaudits as well, becoming a big seller. But, by this point, the band members had gotten busy ravaging themselves with drugs. Axl famously made a concert announcement that Izzy, Slash and Adler better stop dancing with Mr. Brownstone.

## Drugs
During the 80s, Stradlin had been on and off drugs. He hadn't been a junkie when he moved to LA, but he lived with a guy who sold drugs, and, as is so often the case, he tried it "just once", and then was dabbling in it, and inevitably got hooked. Still, he managed to straighten himself after he got busted and quit cold turkey, limiting himself only to alcohol while Appetite For Destruction was in the works. But once he got off tour in 1988, he started using again.

It was while touring with his childhood idols, Aerosmith, that he observed how tight their playing was, and, by contrast, how GnR's music was already playing second fiddle to the rockstar lifestyle. Steven Tyler had told Izzy about his own experiences, pointing out just how bad it could get if he didn't sort himself out. But it would get worse before it got better.

By 1989, Izzy was on all kinds of drugs. GnR's manager once revealed that Izzy went into a 36-hour coma in Japan when he was advised to ditch his entire stash to avoid getting into trouble with the authorities, but chose to ingest it all. And they all were, really. Steven Adler would eventually be sacked for being unable to keep it together long enough to play the drums properly (and having something like 28 overdose incidents). Duff would eventually have his pancreas explode on him, giving his *insides* third degree burns. Ouch.

As for Izzy, he would find himself rounded up for pissing in the aisle of an airliner, in full view of fellow passengers. He'd already spent that flight berating the service, gotten up to use the restroom, found it occupied, and decided the best option was to void his bladder into a nearby trash can. Having answered the call of nature, he promptly went back to his seat and fell asleep. Imagine his shock at being taken into custody just after getting off the plane.

It was made worse because he'd already been busted once before for possession. This time, the judge put him on a year's probation, and told him he'd be subject to random urine tests. Getting clean ceased to be optional.

He checked into rehab, got proper counselling, got clean and sober, and really made it stick. He has been off of drugs ever since. A big part of it, he says, was because he really wanted to.

> "Once I got maybe like a week of sobriety, like actually going a whole week without a drink, I thought, 'oh god, if I can just keep this up' …"

Interestingly, his last alleged drink, one he allowed himself a fair while into sobriety, was with childhood heroes Keith Richards and Ronnie Wood. I wish my life had pub trivia half as cool as this.

## Quitting GnR

While he'd gotten himself clean, Slash, Duff and Steven were still using and still drinking to oblivion. This made life harder because not only was staying clean around junkies difficult, but those junkies also happened to be his bandmates and friends. So he'd turn up at rehearsals and get offered a line of coke.

> "...Slash and Duff, these guys were on my top ten list of guys that might die this week" - Izzy

He'd learnt his lesson, however, and it stayed learnt. I find it seriously impressive because, as they say, 'once an addict, always an addict'. You never cease to remember how good it felt to be on the drug, you just choose not to indulge in it. Rich Roll likes to say something like "no matter how far I drive, I'm never far from driving into the ditch again".

So, although he was keeping a sane head and keeping clean, the craziness of the late 1980s Guns n' Roses lifestyle had started getting to him. The Use Your Illusions tour has its place in the annals of rock music infamy largely due to Axl's antics; he was showing up hours late for nearly every show, walking off the stage at the slightest provocation, or picking fights over everything. So, you'd have the other band members waiting to start the show, while Axl is out at a fireworks festival, having choppers sent to bring him to the concert. And, as Izzy says, those hours pass *slowly* when you're sober. On top of that, he couldn't get the "why" of it all. *"What are we getting at here?"*

Eventually, he even started travelling from gig to gig in his own bus, with his girlfriend and his dog. He became the consummate professional; show up to concert, play your part, leave straight after.

The final straw came during the show in Mannheim, the penultimate show of their European leg. Something upset Axl during the performance of Live and Let Die, so he shouted "Thanks a lot" sarcastically, tossed the mic, and walked off. There's actually a YouTube clip ([here](https://youtu.be/213nBKa8KiM?t=158)) where you can see Izzy going on tilt. He charges over to the mic that Axl tossed, and kicks it furiously in his direction.

According to Slash, he sent a resignation letter the next day to the band manager, saying the upcoming show (Wembley) would be the last one, not even bothering to talk it out with the band members, that was how disillusioned he'd become with the band. (Interesting aside is that Axl *did* show up on time for that last show.)

Of course, there are two sides to every story. If you ask Axl, or even Slash, you might get the idea that, yes, being sober had something to do with it, but Izzy just didn't want something as hyped and larger-than-life as Guns had become. He didn't want the hard work or long nights or the sacrifice for GnR. While recording for Use Your Illusion, for example, he sent in his parts recorded on a four track, that Slash claimed were sloppily done.

He dismisses those claims with a shrug. "That's not Slash talking. That's Axl talking and Slash repeating it. Axl did say the tapes weren't up to GNR standards. Well, in the beginning nobody owned an eight-track. All our tapes were made on a cassette player. Whatever, I'm credited with just about everything I wrote. I will say that Slash was much better at keeping tapes in order. He always labelled stuff." 

So, maybe no smoke without fire.

## Solo stuff
He'd been clean for about a year by the time he quit, and after leaving, he decided to leave everything behind and just travel the US. Soon, though, he was back with his first solo album, the eponymously titled Izzy Stradlin and the Juju Hounds. The album finally gave him his own voice, literally and metaphorically. During rehearsals with GnR, he'd been the one doing a lot of the singing in Axl's absence anyway, so he figured he could probably make it as a singer too. At any rate, having been around Axl for as long as he had, he was loathe to go out and find *another* frontman.

That first album was very retro and straight up rock n roll. And all of his subsequent albums have been the same way. His songs don't have life altering lyrics, don't reveal deep emotional scars or really, anything resembling a pity party. His former bandmate Dizzy (*yeah*) once complained about Izzy's songwriting, saying that he met Stradlin at a studio and was shown the lyrics of a song called 'Toothpuller' about Izzy's visit to the Dentist to get a tooth extracted (I find that story hilarious).

He describes it the same way too: "That's always how I approach songwriting - no big statement, just telling it like it is. Otherwise, you take all the fun out of it".

He's as elusive as they come. He once released an album only for *Japan* (of all places), many are only on iTunes, and he has often refused to tour to promote his albums. He once cancelled his press schedule to go to Hawaii. YouTube searches for "Izzy Stradlin Interview" reveal little. In fact, even his own band member Axl would publicly complain about how Izzy would often be unreachable because he was out riding his dirt bike or racing Baja trucks, or he "had to" go to the desert to do donuts or something.

> Izzy has always been the kind of guy with somewhere else that he needed to be - Slash

Now, there are some people for whom being elusive comes across as an act. Robert Fripp comes to mind (no disrespect to him, he's a bloody legend and I'm fairly confident he really just *is* an intensely private person but it comes across as a bit self-absorbed and *serious*). For Izzy, it comes across more as an "I just can't be bothered".

I've met some people like that in my life. I was once hiking a section of the Appalachian Trail and met a thru-hiker (trail name 'Fish') who was doing his *third* AT. He must've been in his fifties, and he'd completed two PCTs plus a whole bunch of other long-distance hikes. He owned no cellphone, had no facebook, recorded everything on an ancient handycam with a *tape cassette*, and had a wife who understood his desire to be away from everything for months at an end. There was nothing inauthentic about his lifestyle. It was for him and him alone, and not to prove a point to anyone or to deliberately make a statement or be different. And that's how I think of Izzy Stradlin.

In his own words, "When I have something I wanna do, I gotta do it. I like just doing it." And that has been the pattern he's followed all his life. Here today, and gone tomorrow. He'll write a bunch of songs, and just go record them. He has a YouTube channel and a Twitter account, neither updated with anything resembling 'frequency'. He seems to me to be the type of person who has never googled his own name. When you listen to his albums, you almost never get the sense of "Whoa! This stuff is radical!" Plenty of simple chord patterns. Superlatives don't often appear in the same sentences as his songs.

But one gets the sense that that's how he likes it. He's not out to change the world. He was thrown into stardom at a fairly young age, but managed to make it out alive, and was left with the lesson that the huge crowds were for other people, and that his preference was for normalcy, sanity and being unattached, untethered and unburdened.

I've loved this Type-B, unruffled, '*do* go gently into the night' vibe that emanates from the pages of his life story. And this is what rock and roll is to me. There's no need for life-changing lyrics, or making a dent in the universe. "*Don't butt at the storm / with your puny form*" and all that. What it all comes down to, what classic rock n' roll all comes down to in the end, is a sense of carefree joy. Life in the slow lane. And that is what you find in his music. And it sure makes me feel good.

> It's a long and windin' road, I\
> Sure enjoy the view, yeah\
> If you wanna see it, you're so welcome\
> to come along

## Further Reading/Listening

* [1992 interview with *Musician*](https://web.archive.org/web/20120311211209/http://www.chopaway.com/viewtopic.php?id=555)
* [1998 interview with *Rolling Stone*](https://web.archive.org/web/20101225141704/http://www.chopaway.com/viewtopic.php?id=525)
* [2001 interview](http://www.chinesedemocracy.com/forum/index.php?topic=49804.0)
* [Izzy's page on Appetite for Destruction fan website](http://www.a-4-d.com/t89-izzy-stradlin)
* My top five Izzy songs for a new listener:
  * [Shuffle it all](https://www.youtube.com/watch?v=FDtc5q-9Cqo)
  * [River](https://www.youtube.com/watch?v=Xd0Q4tRv1Pk)
  * [Ain't it a bitch](https://www.youtube.com/watch?v=BJZaG4iFk0A)
  * [Seems to me]()
  * [Somebody knockin'](https://www.youtube.com/watch?v=4tB5R48oTbs)