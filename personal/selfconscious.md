# Advice for the Self-Conscious
There are those amongst us who know that the *worst* bike ride is better than the best bus ride. On a bike, you can be cold, wet, muddy and grimy. But on a bus you're surrounded by people. People who watch you, judge you. You feel like you're a model, catwalking on a stage, or that a movie camera is filming every move of yours to display on a big screen. The Spotlight Effect.

For people like us, being in places with crowds is not enjoyable. Dance clubs, fares, shopping centers, buses. These things bring us dread, or at best mild discomfort, as we walk around burdened with the weight of other people's eyes. Well, if you're like me, the following tricks may be of help. I implement these things to varying degrees and with varying amounts of success, but together they might offer some ideas.

**Find 5 interesting things during your commute**  \
The word 'interesting' is vague, by design. You've done your commute hundreds of times in your life. The basics of it aren't interesting. You know all the bus stops, and roughly how many people get on at each one. You know the regulars. These are not novel to you. What might be interesting is the shape of a streetlight, a house with an interesting looking swing in its backyard, or the silhouette of a couple, kissing passionately, glass of wine in hand, shadows cast on the curtain of a window. That last one actually happened to me on a bus ride in Paris. Really like a movie.

I love doing this because it forces my mind to shut up and look. It's also easy to implement, in the meditation sense of being able to return to doing it once you're inevitably distracted. To paraphrase Arthur Gordon from 'Day at the Beach', when you truly look at "...something outside yourself, you have to silence the clamorous voices within. The mind rests."

**Describe the scene like an author would**  \
Imagine you're writing a book about this very moment (even if you're on the john). How would an author describe it?

*"There was fresh snow outside his window, but the lamp cast a warm light over the desk as he sat writing these words. He had a meeting about to start in three minutes, but wanted to get these words off of his chest. The stress of the forthcoming Christmas break weighed heavily on his mind..."*

I've found great value in narrating to myself what is happening to me. In the same vein as the last point, doing this forces you to take in more of the scene. It gives you new eyes. You have to look at more than just the immediate goings-on outside the window. You must instead seek a broader context. What is the temperature like? What's the lighting like? Is it colourful or drab? Crowded or empty? Why? As with most of these tips, it is to help you forget.

What's more, it also makes the day just a bit less mundane and repetitive. It's a tiny nudge towards the fact that my life is also an adventure, however unremarkable it might be.

**Meditate**  \
For the life of me, I can't remember where I read this idea, but it went something like this. If you have a phobia, there are two ways to deal with it. One involves gradual exposure, and the other amounts to jumping into the deep end. The classic example for the deep-end method is, in fact, the Spotlight Effect I mentioned above. The idea is that, say you have an anxiety around crowds and so you avoid people. Well, you could go to the opposite extreme and dress up in the most over-the-top attention-seeking way you could possible imagine. Dressing up as a literal peacock for example. You would quickly realise that statistically, almost no one looks at you. A few would laugh, maybe some would high five, but most would ignore you. Like Jake Peralta when he dresses up as an environmental activist.

My suggestion is a bit easier. You're in the bus, feeling watched. Could you slowly close your eyes? You could even feign falling asleep if you're especially concerned about looking like a lunatic. Once your eyes are closed, you will initially feel everyone watching you. Your brain will still have a mental map of where people are seated, who has line-of-sight on you and all the other inane stuff we have to walk around with all day.

But persevere. Slowly practice proprioception. Ask yourself if you can feel the bottom of your feet. Can you place them even flatter and gentler on the ground? Can you feel the ground? Next, how much can you relax your face muscles? Jaw, cheekbones, eyelids and finally forehead. Then move on to your breathing. Can you focus on the diaphragm?

This really is a cheat code because it has the added benefit of being a pseudo-meditation. You're using the bus ride not to escape anxiety by looking at your phone, but rather implementing some form of stillness.

**How good a witness would you be?**  \
I can't remember the inspiration for this point either, but it was a Reader's Digest puzzle book that we used to own (Actually, it was more a personality test book now that I think about it). In it was a fun little quiz which had us stare at a sketch for five minutes and then turn to the last page. On the last page, it asked us questions about the scene. The sketch showed a crossing where a car crash had occured, and the questions were similar to what a lawyer might ask during a court case. How good a witness would you be.

I've repurposed this advice to a different end. Sometimes, on days I feel especially combative, I try to walk down a corridor determined that I should be able to recognise everyone I pass in a police lineup. This requires you to look at people for a little longer than you would. Normally, one would avert one's gaze the instant it met someone else's. Now, you have to do it for just a bit longer until you've memorised their face, features or other identifying marks.

The idea here is for it to be a little boost to your confidence and your ability to look people in the eye.

**Final thoughts**  \
Ultimately, if you attempt these things, you're preventing yourself from looking at your phone, which is a win in my book.

Now, in the world of 'self help' there is an implicit belief that you will solve things for life. That changes are forever and that each day can be the same as the day before and the day after will be like today. In reality, life is about ups and downs, wins and losses. Seasons. Some days, you will have an easy time of it all. Other days, you will feel like shit and retreat to the comfort of your phone, tips be damned. Maybe weeks will pass and then you will regress. Life is about trendlines, not permanence, so don't worry about it.

In the worst case, you could always bike :)