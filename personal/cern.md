*This is a look back to how I saw CERN as a 21-year-old. I updated it as a 27-year-old. I'm now 35, so these views have mellowed some, obviously, but are correct in their essence. I'll also preface this by stating that my experience is not a proxy for anyone else's. At a place as large as CERN, people will (and do) have all kinds of varied experiences, and this is just the viewpoint of one, somewhat naive, rose-tinted-glasses-wearing ­guy.*

# Prochain arrêt, CERN

I adore CERN. I'm glad a place like it exists. A place where you work at the forefront of science and engineering. The detectors that we work on are, by size, the same order of magnitude as cathedrals, and yet tolerances are frequently measured in microns. You work with some of the smartest people in the field of physics and engineering, and the problems that you work on are rarely uninteresting or repetitive.

It is a place that gives you a *mandated* two weeks vacation for Christmas, aside from the other thirty days that you earn throughout the year. As much as Cadillac [would like to convince you](https://www.youtube.com/watch?v=xNzXze5Yza8) that this is un-American, you have to experience it to understand just how much difference a _relaxed_ end to the year can make. There are showers at work if you choose to commute via bike or if you go for a run in the afternoons. You can buy a glass of wine at the restaurants, and sip it with colleagues while you sit outside with beautiful views of the Juras and the Alps to keep you company.

![](/assets/cern1.jpg)

Around CERN, you can live either in pretty little French villages nestled at the bases of mountains (my preference), or you can reside in Geneva, where the lake is always a short walk away and you can go in for a swim, or to sail, kayak or paddle-board, or take a leisurely Saturday morning cruise, or indeed, just sit by its side, beer in hand, and watch the world go by.

Perhaps I'm painting too idyllic a picture. No place on Earth is quite paradise, after all, but Geneva comes pretty damned close. Closer certainly than what a 21-year old guy from a filthy part of Delhi had ever experienced before.

I first came to CERN at the turn of 2011 to do an internship for my final semester of undergraduate mechanical engineering. My job involved helping to develop a cooling system that would serve as a replacement to the one then being used for the innermost layer of one of the experiments (CMS).

![](/assets/cern2.jpg)

One of the first things my advisor, Hans, told me when I started was *"This isn't university anymore. Not knowing the answers is not the crime. Not asking the questions is"*. As a young, awestruck engineer with iffy fundamentals, this is just what I needed to hear. I shared an office with him and I frequently took him up on that statement, asking him as many questions as I could get away with. Without fail, though, each time he would grab a sheet of A4 paper, start with first principles, and explain everything I'd asked of him until we'd both figured it out.

### Hans
Now would be a good time to describe my advisor. Hans flies aerobatic aircraft for fun. He also flies them in air shows where he does barrel rolls with them. He has, in fact, a license to *repair* his own aircraft. When he finds time off from flying, he likes to ride around his restored, 1956 Harley Davidson. This is a skinny, 6'4" Dutch guy. He told me once of a cross-country trip he'd taken to the US where he'd shipped his motorcycle over, and rode it with his girlfriend. A bike he repaired in his mother's first floor apartment in Amsterdam while still in college. He rode it from the east coast all the way over to the west, turned right into Canada, and proceeded to drive all the way back across Canada. And then went back *again* to Colorado. Not having had his fill, he sold his bike and made his way to South America, where he resumed sightseeing. His heart still aching to seek and his feet still not questioning "whither?", he called up some friends living on the West coast of Africa, and flew over there to bike around a new continent.

He also took the Magic Bus that used to go between Holland and India, classic hippy style. Before the trip, he'd been reassured by his mates that if you ever got caught with drugs in your possession in India, all you needed to do was to produce some *bakshish* (bribe) and the police would give you back your drugs. Well, wouldn't you know it, young Hans got caught with hashish in his possession. He dutifully produced the money, but was infuriated when the cops decided to keep *both* the drugs and the money! Later, somewhere near the Pakistan-Afghanistan border, he had a run-in of sorts with the border patrol.

Hans has just endless such tales to recite from his life, making him an unbeatable drinking partner or lunch companion. And this is all on the side. At his main job, being a mechanical engineer, he is predictably exceptional. He is a treasure trove of knowledge of a staggering array of engineering fields, and has a feel for numbers and ballpark figures.

The thing I'm trying to get at, though, is that he isn't the only one at CERN like this. He's pretty unique, yes, but, for example, a physicist I knew happened to have developed a significant piece of software used widely in his field, was also so good at sailing that he *judged* national level competitions. Another had secured a financial grant from the Swiss government to allow him to keep building electric bikes in his shed. A third is one of the inventors of a cooling system used *both* at CERN and on the International Space Station (the system that I work on), and also owns a farm in Holland, complete with ill-mannered goats that try to make their escape every chance they get.

![](/assets/cern3.jpg)\
*The whiteboard of a random physicist whose room I walked past*

And yet, I don't often see people work long hours, be flustered, or generally overwork themselves to death. Of course, CERN isn't all fun, all the time. There is stress. And there were plenty of sleepless nights, I'm told, when the LHC was being built, or when the [Helium leak](http://www.universetoday.com/18390/helium-leak-forces-lhc-shutdown-for-at-least-two-months/) happened. But overworking is not the norm. Instead, people tend to work reasonable hours as a matter of course.

### The Lunch Hour(s)
Lunch was held pretty sacred during my entire time at CERN. Most days, at 12:00, Hans would say "Time for lunch?", and we'd head to the restaurant. During the warmer months, we would try to eat outside as often as possible because of the aforementioned views of mountains.

These lunches were exactly what I'd pictured European lunches would be; relaxed affairs with good conversation and no smartphones to be seen. Sometimes the conversation would be technical stuff, although nothing was off limits: world economy, Fukushima (this was 2011), history. But usually technical stuff. I'd ask Hans about his motorcycle, or about flying small aircraft, and what that was like. After an hour of this, someone would ask "Time for coffee?" and we'd head to the coffee counter and resume talking for another hour. Some conversations wouldn't even end *then*, and we'd get back to our desks to do some fact checking or continue the discussion.

A mid-day break where you have great conversation is a seriously underrated form of stress busting. It helps so much in letting you forget the worries of the first half of the day, and helps you get a clean(er) slate for the second half.

### CERN's Campus
...is stunning.
\
\
\
\
\
![](/assets/cern4.jpg)
\
\
\
\
\
_...with exceptions_
\
\
\
\
\

It's spread out primarily over two sites. The site in Meyrin (a Swiss village just outside Geneva) straddles the Franco-Swiss border (I used to love mentioning how I'd cross borders just to get some food), and the other in Prevessin (firmly in France) is smaller. It get tiresome to repeat after a point, but it's hard to overstate how much of a difference a beautiful campus makes. I can see mountains from my office window. This something I try hard not to take for granted, but it's so easy to get used to. Whenever I'm feeling fidgety or anxious about my work, I can take a refreshing walk outdoors. Compare this to working in Delhi, where offices have to go to great lengths to keep pollution and noise out, and stepping outside into the heat, humidity and dirt is a masochistic endeavour.

![](/assets/cern5.jpg)

### Sense of Humour
This was the starkest contrast I found between the work culture in the US and Europe. In the US, even though I worked in academia, things were *stiff*. We had a Weekly Update meeting on Tuesdays. We had a Group Meeting on Wednesdays, separate from the Weekly Update, to discuss the progress of the *rest* of the group. Monthly Updates had to be submitted. We had twice-a-year Consortium Meetings where our sponsors came to see what we were up to. There were PowerPoint templates. There were conventions on how our Excel graphs should look like, and whether or not we should use periods in our bulleted lists (we shouldn't). None of this exists at CERN.

I should clarify that I'm not saying the former is wrong. In many ways, I prefer the former. I prefer to have someone else be able to tell me exactly what they need, and to have conventions established that encourage consistency. I prefer explicit over implicit. But our group at CERN is different. PowerPoint slides here use Red, Blue and Green fonts *on the same slide*. My colleague showed up to his conference presentation looking like this:

![](/assets/cern6.jpg)

Inspiring me a little bit too:
![](/assets/cern7.jpg)

The relaxed atmosphere means that, during talks, people find a way to inject a touch of humour:

![](/assets/cern8.png)
![](/assets/cern9.png) 
![](/assets/cern10.png)
![](/assets/cern11.png)

There's also that infamous love affair with [Comic Sans](https://home.cern/about/updates/2014/04/cern-switch-comic-sans). As much as every aesthete out there hates Comic Sans, there *is* something endearing about Science Man or Science Woman being too busy to give a crap about it.

#### Final thoughts
So this was what CERN was to me, as an undergraduate engineer in 2011. I fell head over heels for this "University for Adults" vibe that CERN held, and the very European lifestyle that people in Geneva adopted. The hiking or skiing every weekend, the slow lunch and slow coffees, the "make everything beautiful" attitude, with flowerpots on windowsills and ornate metalworking even on streetlamps. I was unpaid when I first came here, and my project was only five months. But it was long enough for me to catch the bug. I vowed to return one day, in a paid position that I had earned. I knew that Hans was meant to retire in 2019, and that that was the year I'd turn 30. So it was a nice number to aim for. But here I am, 3 years in advance. Hopefully to be *done* by the time I'm 30.

Has it lived up to my internal hype? Well, as you grow older, life gets more nuanced, so the rose-tinted, misty-eyed youth has been replaced by a less misty-eyed adult focussed more on the job itself, rather than how glorious CERN is. But the answer is still a pretty categorical "Yes". I walk past busloads of people who've come to *visit* where I work! I smile as I walk past them and badge my way in. I sometimes go for a walk around the campus, and there is this one road that runs next to vineyards. You can see *both* the Juras and the Alps from that road, and you can see Geneva in the distance. It is unbeatable, and unfailing in helping me get my thoughts together. There really is nowhere else I'd rather work, and nowhere else I've worked has been as good as being here.

Again, there is no paradise on Earth. But this comes pretty damned close.