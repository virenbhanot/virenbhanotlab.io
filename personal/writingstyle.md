# A note on the writing style
I read The Shallows by Nicholas Carr and came away scared at how the internet was shaping our reading habits. In particular, we are constanly skimming, and never giving the text our full attention.

The fact that the web is designed to be a bunch of hyperlinks means that, on any page, there are tonnes of such tantalising exit ramps off the highway taking you to other corners of the internet. Some authors even actively embrace this, in an effort to keep site engagement high.

On this website, I will try to have a minumum of hyperlinks. The reading experience should have minumum distractions. When necessary, I will inculde extra information in references at the bottom of pages. In my own reading, I use uBlock Origin ruthlessly to block out parts of my frequently visited webpages that I do not want to see (like Share buttons etc.). I also often read longer texts in full screen. I highly encourage you to try these also.