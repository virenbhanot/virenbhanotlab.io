@def title = "How this blog is built"
@def lang = "c++"

# How this blog is built
"How I blog" is a post required off of every modern self-respecting webmaster (remember that term? Back when we'd put <marquee> tags on everything). So here's mine.

## Franklin
This website is built using [Franklin](https://github.com/tlienart/Franklin.jl). It is a static site generator built in Julia, and fit perfectly for technical blogging. If you don't know static site generation, it's the future, man! The new thing. Same as the old thing. Modern websites are bloated, overly so for the needs of a simple blog. We can simplify greatly; instead of having webpages be dynamically generated each time someone accesses them (how Wordpress works), we keep a bunch of HTML pages on the website pre-made, ready to be accessed. These pages are then just dumb files arranging elements on screen, which makes for fast loading, and also means that you can serve more people for the same bandwidth.

But a minimal site doesn't mean compromises on the important stuff. Franklin comes pre-equipped with the ability to highlight code. For instance, here is some C++ code:

```
#include <iostream>

int main() {
    std::cout << "Hello World!";
    return 0;
}
```

It also includes support for Latex equations: $E = mc^2$

Both of them work out of the box. While syntax highlighting and latex equations are by no means unique to Franklin, having them be baked in is something I quite like in all static site generators.

Once you really get deep into it, though, it has support for a lot of other useful scientific-computation-y stuff. For instance, the ability to include Plotly plots.

Finally, more than anything, I've had such a curiosity about Julia for a long time. It seems like a very promising language. It's open source, quick, and designed from the ground up for Scientific Computing. Even more, they've got the incomparable Chris Rackauckas working on their Differential Equations package. Seriously, if you ever want to feel bad about yourself and all that you've achieved, take a look at his [CV](http://chrisrackauckas.com/assets/Professional/ChrisRackauckasCV.pdf). He's even got a Youtube channel that's infinitely superior to mine, by virtue of the fact that it actually educates people.

## Version Control
The blog is hosted on Gitlab Pages. Gitlab Pages are similar to Github Pages, in that your blog is basically a version controlled repository. That means that the sausage making is visible, and there is a zen-like feeling of accepting things that I get from it. It also gives me a straightforward venue for fixing typos or incorporating feedback. Gitlab pages and Github pages are very similar, but CERN uses Gitlab internally which is a psychological, if not practical, nudge towards getting me to use it.

## Writing
The posts are all written in Markdown using Obsidian. Obsidian is great. It is, on the surface, a Personal Knowledge Management (PKM) tool, but it is also the best markdown editor for Windows I think. PKM is all the rage right now, and for good reasons. I'll save that line of thought for another time, but the main idea is you create your very own wikipedia, with backlinks between different articles/snippets, and generate interesting graphs to develop patterns between the different places.

![](/assets/franklin_2.png)

I don't particularly care about the backlinks part though. The reason I like it is first and foremost it's such a beautiful place to spend time. There are countless markdown editors available, all of them 'hackable', but I don't want to spend time fiddling with my tools. I want the default typeface to be good looking, I want the line spacing to be spot on and so on. I don't want to be burying myself into CSS files, or writing custom scripts to make vim linewrap properly. "People don't want quarter inch drills, they want quarter inch holes", as the saying goes.

Even better, it's available on both Linux and Windows, covering all my use-cases.

Also, I always wanted markdown to have live previews, aka a sort of bastardised WYSIWYG hybrid. You get that with Obsidian, as a sort of partial preview, with the full preview being a hotkey away. You can even display them together. The "Edit" mode (on the left in the screenshot below) already includes basic syntax highlighting. But the Preview pane really can take care of everything. Live rendering Latex, syntax highlighting, highlighting quotation blocks, HTML sections, the whole lot.

![](/assets/franklin_1.png)

And it's free!

So, in conclusion, I can wholeheartedly recommend this solution to engineers or other tech people who want to write technical blogs. There is a whole community of Julia Bloggers on, ahem, [JuliaBloggers](https://www.juliabloggers.com). Julia, in general, has a wonderful community eager for more people to join. Jump in!
