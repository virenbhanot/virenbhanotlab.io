# I use Arch, btw
*Installing Arch on the Surface Pro 3*

When I bought the Surface Pro 3 (2014), I was pretty minimalistic (still am), and favoured having fewer possessions. It was only a short jump from there to also wanting *lightweight* things that are easy to carry around. A Magnesium laptop that weighs just about 1 kg is, therefore, easy bait for a person like me. In that same spirit of less, I bought the version with bottom-shelf specs; 128 GB, 4 GB of RAM and an i5 processor from that era.

Over time, it's proven to be wonderful in the sense of "can't-tell-if-I-still-have-it-in-my-backpack", but not so much in terms of how well Windows worked on middling specs. And then my touchscreen started detecting phantom touches, so the time had come to do something. I decided to try Linux, because it seemed that Microsoft wanted more and more and *more* data all the time. Fedora, Ubuntu, Mint, I tried them all, but eventually found some stability in Manjaro i3wm.

In a fit of recent exploratory motivation, though, I've decided to run Arch on it. In an even bigger leap of madness, I've decided to forgo X11 completely (not even XWayland) and go straight for Wayland. The SP3 has a gorgeous, high PPI display, even in 2022. It felt fitting to move to something more modern that supports fractional scaling and so on. What follows are some key installation steps, and my configurations to get this set up.

## Before Installation
I'm not running Windows, so no dual booting shenanigans necessary. I have a second dell laptop to look at online help, and to create the bootable USB.

* Rufus v3.17 to create a bootable USB with Arch Linux v2021.11.01.
* MBR partition scheme, BIOS/UEFI targetted system, FAT32 fsystem, 4096 Bytes cluster.
* Write in ISO mode.
* I get a warning saying something like "Syslinux 6.04/pre2 but application only includes Syslinux 6.04/pre1. Download ldlinux.sys and ldlinux.bss?" Yes.

## Base Installation
The [Arch install guide](https://wiki.archlinux.org/title/Installation_guide) on the wiki, and DistroTube's [installation video](https://www.youtube.com/watch?v=PQgyW10xD8s) were my sources for doing the base install.

Step 1. With your Surface in the off position, hold down the volume up key. Press and release the power key while keeping the volume up key held down. The moment you see the Surface logo, let go of the volume up key. This should boot you into the BIOS menu. From there disable both the Secure Boot and Trusted Platform Module thing. You will, from here on out, always be greeted by that hateful primary-colour red splash screen each time you turn the machine on. I also disabled the mic, the front and rear cameras, and the bluetooth, because I plan on using this machine strictly for learning programming, and not for anything fancy. Anyway, do all this and turn the machine off.

Plug in the USB and boot. Select the "Arch Linux install medium" option.
Get to the `root@archiso ~ #` prompt.

I use programmer's dvorak as my keyboard layout, so:
`loadkeys dvorak-programmer`

```
iwctl
station wlan0 connect CERN
exit
```
Verify that internet connection works: `ping google.com`

`timedatectl set-ntp true`

`fdisk /dev/sda`\
pressed `d` a bunch of times to delete all 3 previously existing partitions from a botched install of mine.
`n` to create new partition. Keep default partition number. Size of part `+550M`
`n`, `+4G` swap of 4GB, `Y` remove swap signature (I had a previously installed distro)
`n`, give it remaining space, `Y` remove signature
now change type of partition
part1: `t` -> `1` -> EFI System
part2: `t` -> `19` -> Linux Swap
part3 is fine. No change needed.
`w` to write, and it auto exits out of fdisk

Now we need to Format our file system
```
mkfs.fat -F32 /dev/sda1
mkswap /dev/sda2
swapon /dev/sda2
mkfs.ext4 /dev/sda3 //"Extend4"
mount /dev/sda3 /mnt // Mount sda3
```

`pacstrap /mnt base linux linux-firmware`
Should have probably added base-devel here, which would give me things like `which`. Should also have added man-db here? Because although the manuals were installed, the binary wasn't.

Generate file system table
`genfstab -U /mnt >> /mnt/etc/fstab`
`cat /mnt/etc/fstab` should give some kind of output
`arch-chroot /mnt`
This should change the prompt to `[root@archiso /]# _`

`ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime` because I live in Switzerland (well, France, I'm a frontalier).
`hwclock --systohc`

```
pacman -S vim
vim /etc/locale.gen
```

Uncomment the `en_US.UTF-8 UTF-8` line and then:

```
:wq
locale-gen
```

```
vim /etc/locale.conf`
LANG=en_US.UTF-8`
:wq
```
This makes language persistent.

```
vim /etc/vconsole.conf
KEYMAP=dvorak-programmer
:wq
```
This makes the keyboard layout persistent.

```
vim /etc/hostname
sp3
:wq
```

```
vim /etc/hosts
127.0.0.1	localhost
::1			localhost
127.0.1.1	sp3.localdomain	sp3
```

```
passwd
*****
*****
```
hunter2

```
useradd -m vb
passwd vb
*****
*****
```

```
pacman -S sudo
visudo
```
uncomment wheel group `%wheel ALL=(ALL) ALL`

`:wq`

Note: NOT the NOPASSWD line. The one before it.

Set up Grub properly:
```
pacman -S grub
pacman -S efibootmgr dosfstools os-prober mtools
mkdir /boot/EFI
mount /dev/sda1 /boot/EFI
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
grub-mkconfig -o /boot/grub/grub.cfg
```

Finishing up:
```
pacman -S networkmanager git
systemctl enable NetworkManager
```
should tell you "Created symlink..." something.

`exit` to exit from chroot\
`unmount -l /mnt` to unmount\
`shutdown now`Disconnect the live USB and then start the PC again.

## Networking
Despite the `systemctl enable`, NetworkManager didn't start up automatically.

```
systemctl start NetworkManager
nmcli device wifi connect CERN
```
wait for a second for it to connect
check the ping: `ping google.com`
```
systemctl enable NetworkManager
reboot
```
Try the ping again, now should work. Remember NetworkManager takes a second or two after log in to connect to the wifi. So be patient. Also, if your wifi/password has special characters:
`nmcli device wifi connect "wifi name" password "pass with spaces"`

Next, the Marvell 88W8897 wifi chip on the SP3 has this annoying habit of being put into powersaving mode by Linux. Meaning the internet gets disconnected for good when the laptop is on battery power, solvable only by either resetting mwifiex_pcie, or rebooting. To solve this:

```
sudo touch /etc/NetworkManager/conf.d/01-wifipowersave.conf
vim /etc/NetworkManager/conf.d/01-wifipowersave.conf`
```

add the text:
```
[connection]
wifi.powersave=2
```
:wq
The 2 means _don't_ do powersaving. 3 is _do_ power save, while 0 is _default_.

Since I've done the above change, I've never had the problem of the wifi dropping. Nevertheless, as a backup, it's worth creating a shell script for resetting the wifi anyway:



## Ranger
`sudo pacman -S ranger`
installs: libnsl, python, ranger
Now, we have a file manager. With vim keybindings to boot.

## man
For some reason, my Arch install couldn't do `man something`. Because I didn't install base-devel, I guess.

man are located in "/usr/local/man" and in "/usr/share/man". The latter is where all the .gz manuals were, inside man1 through man5 or whatever.

`sudo pacman -S man-db`
Which then installs the binary to /usr/bin/man

## base-devel
`sudo pacman -S --needed base-devel`
installs: autoconf, automake, binutils, bison, fakeroot, file, findutils, flex, gawk, gcc, gettext, grep, groff, gzip, libtool, m4, make, pacman, patch, pkgconf, sed, sudo, texinfo, which

## yay
To build yay:
```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -sic yay
```
The -c flag will clean up the directory after yay is installed.
I even went ahead and removed the folder itself:

```
cd ~
rm -r yay
```
Complaints about "remove write protected file" in .git folder blah blah? I said 'y' twice.

Now, at this point, we have a functional Arch install, without any desktop environment. On my machine, the keyboard backlight media key worked, but the screen brightness control keys didn't (probably because it had nothing to control the brightness with). The default scaling etc. all looked pretty OK. The audio probably also didn't work, although I'm too stupid to figure out whether it did or not.

# Desktop Environment (Sway)
Followed the [guide](https://www.fosskers.ca/en/blog/wayland) by Colin Woodbury. You should probably do the same.

`sudo pacman -S sway alacritty waybar wofi qt5-wayland glfw-wayland`
Asks for which fonts I want to install. The gnu-free-fonts look abysmal to me, and they are the default, so careful here. The other options are Noto, Bitstream Vera, Croscore (?), DejaVu, Droid, IBM Plex (which I quite like actually) and Liberation. Apparently, DejaVu is Bitstream Vera, but with unicode character support, and is effectively the Consolas of the Linux world, so I went for it (option `5`)

Opengl-drivers were available from `mesa` and `nvidia-utils`. Chose `mesa`.
Looks like a whole bloody bunch of xorg things got installed.

Did `sudo pacman -Rs sway alacritty waybar wofi qt5-wayland` to reinstall and try to figure out why all of them were needed. 

Turns out `qt5-wayland` needs a shit load of 'x' things.
xorg-xprop, xorg-xset, xorgproto etc.

## Brightness control
`sudo pacman -S brightnessctl`
Keyboard brightness control is done using this.

## F.lux
Doesn't exist for Wayland. Neither does Redshift (at least not trivially enough for my taste). Luckily, `wlr-sunset` is phenomenal.
`sudo pacman -S wlr-sunset`

## Sway configuration
### Inputs
**Touchpad**
I like to tap instead of click the touchpad, and strongly prefer the triple finger middle button click emulation. I also like Natural Scrolling, wherein the Touchpad swiping resembles a Touchscreen, that is, if I swipe *up* on the touchpad, the *scrollbar* goes *down*, aka I go *lower* on the page.



...


There is more stuff that will be added here as time passes. This'll be a kind of living document.