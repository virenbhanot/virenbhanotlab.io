@def title = "Hello World"

The world desperately needs another weblog[^1]. Specifically, it needs a weblog that talks about Life As A Mechanical Engineer.

I've always looked at programmers with a certain degree of envy. It seems that the profession of programming is blessed with countless wonderful forums where programmers can delve into the nitty-gritty of their field. HackerNews, Lobste.rs, /r/programming, and even the 'Software Engineering' StackExchange, there are tonnes of places where programmers can have meta discussions that are interesting and illuminating.

By contrast, I do not know of such places existing for other fields of engineering. This is a shame because these other fields are just as interesting, with people who are just as creative and brilliant. And yet, the kinds of informal and meta discussions that I seek are relegated largely to  conference lunches. Those are important, sure, but they don't help a newcomer get a 'lay of the land', as it were, or to see what life is like in the field.

A good step towards addressing this would be more people blogging. Random tips, tutorials, post-mortems... anything. This blog is my attempt at such a website.

[^1]: I love Maciej Ceglowski. This is a rip-off of [his hello world](https://idlewords.com/2002/05/hello_world.htm)
