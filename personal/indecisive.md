@def title = "The master of the indecisive moment"

# The master of the indecisive moment
_This is a very old article._

Mark Knopfler’s album introduced me to him. The album’s cover, more specifically.

![](/assets/ee_ragpicker.jpg)

The man who took this picture is a Paris-born American with Russian parents. His name is Elliott Erwitt. Erwitt, now 82 (I wrote this in 2010), has had a long and prolific career as a photographer. Over the course of that career, he has not only photographed some great moments during some great moments, he has also captured some of the most poetic. Dealing almost exclusively with black and white photography on film, a lot of his photographs have been clicked using his beat-up workhorse of a camera, the Leica M3 with a simple, no-frills 50 mm lens.

![](/assets/ee_leicaM3.jpg)

_Showing, once again, that it’s about the artist, not the instrument_

He has been called the master of the “decisive moment” – that moment in which the subjects tell a story. The Kodak moment if you will.

Some of his most decisive moments have been capturing the desolate figure cut by Jacqueline Kennedy at her husband’s funeral, as she is left alone for a moment, or, perhaps his most recognizable photograph, that of Nikita Khrushchev being poked in the chest by a rather imposing looking Nixon.

![](/assets/ee_jacquelineKennedy.jpg)
![](/assets/ee_dickNixon.jpg)

And then this photograph of a black man drinking water from a water fountain for “colored” people, glancing in the direction of the sparkling-clean white man’s sink.

![](/assets/ee_sink.png)

Simply stunning, isn’t it?

However, what attracts me most to his work is his ability to capture the simplest of human emotions and romanticize them brilliantly. The simple scenes which, while humdrum and frequently encountered during daily existence, allow you to pause for a moment and reflect on all that is good about life but doesn’t cost a fortune.

![](/assets/ee_coupleAndSmug.jpg)
![](/assets/ee_coffeehouse.jpg)
![](/assets/ee_manDogConversation.jpg)

The expressions of all three, the husband, bride and the other guy, all speak volumes. A man who’s just finished with his daily ritual of having a coffee at his favorite coffee-house. A man who is still having coffee and sharing a moment with his dog.

![](/assets/ee_france.jpg)

This last one is a particular favorite of mine. The scene says nothing, and yet says so much. Captured explicitly is simply a man riding a bicycle with a young boy at the back. What is left out is the scene at “home” where the father tells the son to come along. And what is left out is what happens when they reach their destination. What the scene says is so much. The son wearing almost the same clothes, down to the cap, that his father wears. The love that the father feels for his son bursts through somehow as well. Little details like these.

And it is these scenes which, while purportedly dealing with “the empty spaces between happenings”, still have so much to say. This has led him to be called, not without a hint of contradiction, the master of the “indecisive moment”.

Of course, Erwitt detests wordy, pseudo-intellectual descriptions of his work. He prefers to let the people viewing them experience them through their own eyes. To him, everything – the lens, the camera, printing techniques, lighting, Photoshop – all take a back seat to the content of the photograph. The scene is everything, and it’s all about what the viewer takes from it.

Great artist, then.

PS: Of course, no description of Erwitt would be complete without mentioning his photographs featuring dogs.

![](/assets/ee_jumpingDog.jpg)

![](/assets/ee_dogMan.jpg)

In the first one, the dully-dressed owner is, one can imagine, leaving his house to visit somewhere, spiritless and completely devoid of enthusiasm. The dog, meanwhile, is in stark contrast. Yapping, all excited and exuberant like nothing else on earth.

The second photograph is a rather literal description of “dogs resemble their owners”. Enlarge it to observe it.

*Edit: RIP Mr Erwitt.*