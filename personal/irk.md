# Things that irk
I've intentionally excluded low-hanging fruit like "could of" from this list. That's too easy.

**Myriad of**\
Ditch the 'of'. Myriad possibilities. Myriad errors. Myriad irritations.

**Biweekly**\
Fortnightly.

**Purposely**\
Until recently, I didn't even believe 'purposely' was a word. "Deliberately", "intentionally", "purposefully" or "on purpose" had always sufficed.

**Could care less**\
Wish I could.

**Irregardless**\
(Posted without comment)

**Overwhelm**\
Overwhelm is a verb that people use as a noun. I hate it. Apparently, it has been *used* as a noun since the 16th century, so shows you what I know, but man I hate it.

**Melancholy**\
You're feeling *melancholic*. There's something melancholic about the song. I'm filled with melancholy that dictionaries don't support my view that we should never use melancholy as an adjective or adverb.