@def title="Hikes I've been on"

Here's a list of hikes/outdoor stuff I've done. Some of them, someday, will have trip reports.

## 2015
"Hike" from Harper's Ferry to Ed Garvey shelter\
"Hike" in Shanandoah National Park

## 2018
Fort L'Ecluse via ferrata\
Murren via ferrata\
Curalla via ferrata\

## 2019
[Huemul Circuit](/personal/outside/2019-huemul)\
Lac Emosson\
Chamonix to Triente\
[Haute Route](/personal/outside/2019-whr)\
Moleson via ferrata\
Fort L'Ecluse via ferrata\
Kandersteg via ferrata

## 2020
Hochmatt Gastlosen\
Evettes via ferrata\
Lac Blanc hike\
[Alta Via 1](/personal/outside/2020-av1)

## 2021
Fribourger Voralpenweg\
Lac Leman on bike

## 2022
Jura overnighter\
Lugano trekking tour\
Mittelland route bike tour (didn't finish)

## 2023
[GR 20 (Corsica)](/personal/outside/2023-gr20)

## 2024
[Pacific North West](/personal/outside/2024-pnw)