# Musings on Cold Turkey
In India, we have some Very Important Exams™ that take place when we get to Grade 10 and Grade 12. Instead of each school setting its own end-of-year exams, a central body sets it for *all* schools, as a way to standardise test results. The idea is that the marks you get are then easier to judge when you apply for university, because all kids give the same exam. And indeed, the 12th grade marks you get matter a lot. To help you prepare for them, schools typically stop classes right around Christmas, leaving you three months to prepare well.
\
\
\
I spent my three months playing Counter Strike.  
Against Bots.
\
\
\
Both my parents were working, and my brother still had school. So from around 07:00 to about 15:00, there would be no one in the house but me and my grandmother. I spent my time playing Counter Strike: Condition Zero. The whole hog in terms of true-to-stereotype; I'd jog first thing in the morning to the corner store to buy Mountain Dew and Lays, I'd hurry back, eager to be reunited with my PC, I'd turn on de\_dust2\_cz and play non-stop until my brother came from school. Then I'd continue playing anyway, because fuck him, his turn will come at ten-to-never. I'd maybe make an exception for when my parents came home, but any moment I was not in front of a PC I was fantasising about being in front of it.

I didn't bother with LAN servers. I'd just play against bots. You can add a prefix to bot names, so I'd give them stupid names. I'd often be playing against "*Numbskull*\_Adrian", "*Numbskull*\_Kurt" etc. I'd skin my weapons, change the sounds and their look. Eventually, I could only play against expert bots, since the rest was too easy. Variously, I also played Vice City, San Andreas, Fifa 2005 and Football Manager. But CSCZ, I would spend 19 hours a day playing.

I was playing *so much* counter strike that I started suffering from the Tetris Effect. I started visualising enemies around corners. Real life started resembling a CS map. I started physically strafing around corners instead of walking around them. My head was in a fog most of the time.

Needless to say, I didn't do well in my exams. 

No matter though, I still managed to get into *a* university for Mechanical Engineering. But this pattern continued through college. Our university furnished all new students with (pretty outdated) laptops. Mine was full of pirated games. I'd play until the day before the exam (and sometimes stress-play on the day before). My friends made fun of me because I was constantly on the merry-go-round of uninstalling games, failing to resist the temptation, and then reinstalling them the next day. This went on for years. For the best part of a *decade*. Through four years of Bachelors in Engineering, through two years working in industry, and all the way through to grad school in Maryland.

Rock Bottom came the day I found myself not going to my research lab, skiving off to play video games. On a Friday before the weekend.

Something clicked that day because it occurred to me that I was a bona-fide loser. I was wasting my short life on Earth in pursuit of an activity that had an iron grip on me. A vice-like hold that I. Just. Could not. Resist. I felt helpless and broken and defeated and ashamed and humiliated and irresponsible and incapable. I walked around with a deep pit in my stomach. A hollow feeling that everything was off.

> I sometimes think I see his face -\
> The man without a voice.\
> The man who walks another place,\
> Divided by a choice.\
> \
> Perhaps he never made the same\
> Mistakes across the years -\
> He never felt an addict's shame\
> In broken pride and tears.\
> \
> Perhaps he wed the perfect wife,\
> And sailed across the sea.\
> Perhaps he led a better life.\
> Who cares?\
> He isn't me.\
> \
> - /u/poem\_for\_your\_sprog

Somehow, this time, it stayed. I uninstalled whatever video game it was that I was playing that day, cold turkey, and have never looked back*. In the days that followed, the "soy un perdedor" feeling way overpowered any desire I might have had to go back to video games.

But in the weeks and months that followed, I realised the sheer power of cold turkey. There are two types of people on Earth. People like me with addictive personalities. We do things dialed up to 11. Nothing is half-assed and self control is a myth. I'm either on something or I'm not, no middle ground. On the other side, there are people like my brother who would play video games, even when he was young, and then get bored and want to go hang out with friends. He has an XBox, but it exists solely to de-stress, and doesn't hamper his social life.

For people like me, quitting dramatically is fantastic. It's the way to go. It's the *only* way to go. When I'm off something, there are no moral dilemmas, no decisions to be made, no willpower to be exercised (after the initial expense). It's exciting to quit, there's a sense of mission, of purpose, of self-improvement.

In the years that followed this, I used the same process for many things which have all benefited me. 

I quit Social Media (aka Facebook) in 2013, and haven't looked back. I briefly had a Twitter account, but Twitter is a cesspit so quitting it was easy. I never *had* an Instagram account so I don't know what I'm missing there, but I'd venture it's not much. I'm too old for TikTok. I was addicted to Reddit for a bit, but have you been to the homepage recently? Nowadays, it's hacker news and YouTube, but honestly, YouTube thumbnails grate and rankle, and I stopped enjoying the free-market obsession on HN a while ago. Knowing me, I wouldn't bet against them not being in my life in short order.

Not having social media has been a blessing. It has made me realise the impotency of heated drawing-room arguments. I still get riled up by them, mind you, but I reckon I get riled up less than people who get *all* their talking points from Twitter. I just don't think a Nobody sitting in a small middle-class house drinking cheap wine has much power, so I don't bother much about worldly events. I don't follow the news, and I don't enjoy discussing Elon Musk.

Quitting cigarettes was the next thing. I used to smoke a fair bit in the US. I'd take an espresso and walk down with my friend Dan outside the campus (a no-smoking campus) for a smoke. Marlboro Golds. Halcyon days, so *non, je ne regrette rien*. But it went up a notch in terms of quantity, and several notches in terms of self-harm when I returned to India after grad school. I was waiting for visas to start working at CERN, a seven month wait, and soon was up to around 10 cigarettes a day. And these were shitty Indian cigarettes packed with enough tar to be useful in emergency road repairs. I was going out every chance I got, and impersonating a chimney each time I did.

So when I came to Europe, again, I quit cold turkey. I had an app that could tell me where my body was at in terms of recovery. You know, "Congratulations, now your bloodstream has no more nicotine!" or "Well done! Today your lungs only resemble that of a 40-year-old consumptive" etc.

What has been the impact of quitting things like this? I think I'm more Well-Adjusted, and this is not something I take for granted. *Choices I have made* have lead me here. I could easily have ended up an asocial introvert who spent all his time playing video games. Instead, I've been on thru-hikes, I've climbed mountains, I've learnt to swim, I've attempted to learn to ski. I've played music. With people too. I've *made* music. I've made videos about my hikes. I've read books (though not many). I've been able to talk to people, and ask them questions about their life. I've made friends, I've been in relationships, and I've made memories.

There is a lot to be said about the importance of the flywheel effect on life. *Because* I hadn't been playing video games for four years, it was easy to go to the fifth year. It was already habitual by then. *Because* I never smoke, it's easy to continue to not smoke.

(Side note: I had a moment of weakness recently when the smell of my colleague's Marlboro Gold was too much of a nostalgia trip. So I asked him for one. And then another. But it tasted off, and it tasted harsh, and I spoke like Batman the whole next day, so it was a valuable lesson.)

This positive experience with quitting has also lead me down to many mini-challenges. I quit coffee for 30 days, and had a great time recovering my sleep. I quit sugar for 30 days, and was devastated at how much I'd been eating it. All this year, I'm in the process of not eating meat, and not buying any new clothes, and wow is it a nice feeling.

When you're not allowed to buy any new clothes, you're not allowed to be unhappy with your wardrobe. Your wardrobe is neither good nor bad, it just is, and you have to make do with it. This is *amazingly* freeing. It's SUCH a pleasant feeling to just shut up for a year and not have the right to complain, and instead have the shameful realisation of how many clothes even a self-professed minimalist like me has. Despite it being a no-clothes year, I'm actually still getting *rid* of clothes. Shitty plastic T-shirts given away at races, old baggy sweaters that it's no longer cold enough to wear. Stuff like this.

So, moral of the story is, quit things! Cold turkey. Make bold proclamations and try out drastic reductions. Self control is bullshit. Try to make a dramatic choices because they just might lead you away from a shitty destiny, and towards something much brighter.

_\***Never looked back**: every time my brother comes over, I indulge myself by playing the games on his PC. But I refuse to install anything on my own computers. Indeed, I don't even have a personal Windows machine anymore._