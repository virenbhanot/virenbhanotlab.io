@def title = "Guitar George"

# Guitar george
_This is a very old article._

I have always learned the meaning of words in context. If I read some word in a book whose meaning I didn’t understand, I wasn’t one of those who’d rush to the dictionary to figure out what it meant. I’d look at the sentence, get a rough idea, and move on. While this helped me greatly when I was giving my GRE, it did lead to the occasional misunderstanding of words. For the longest time, for instance, I used to think that ‘pristine’ meant ‘beautiful’ or ‘serene’ or some such. I used to also think that ‘prolific’ meant ‘excellent’. Only later was I to realize that it meant productive.

That word, though, is not necessarily a trait that makes for great artists. Indeed, it could be argued that ‘prolific’, like ‘popular’, can sometimes be used to filter commercial ‘sell-outs’ from ‘real’ artists. Mark Knopfler, however, is a man who’s managed to be both prolific and popular, while still making music that has soul.

*(Fair warning: When I like someone, I tend to fawn over them. This post is a long, drool-filled gushfest.)*

Most people know of Knopfler as the late-70s/early-80s artist who insisted on wearing hideous headbands while writing gems like Money For Nothing and Sultans of Swing with Dire Straits.

![](/assets/gg_haha.jpg)

Those who haven’t been following him might be unaware of just how much good music his solo career has produced, or that his latest album came out just last year (2015) (_Again, I wrote this a while ago_). And that album is as much of a gem as any of his previous ones.

I have a good friend from high school who once asked me if I’d heard this song called “Boom, like that” by Knopfler. This was back in college, and I sort of knew who he was, because even back then I was a fan of Dire Straits. But “fan” meant I knew Brothers in Arms and Sultans of Swing. So I checked out the song, and to say I was impressed would be an understatement. If you haven’t, [you should check it out](https://www.youtube.com/watch?v=0sYK2RwH5E8). The song is about Ray Kroc, the man who attained considerable infamy while amassing a considerable fortune making McDonald’s the world’s biggest fast food joint.

Of course, being Indian, I had no context about who Ray Kroc was, but I managed to learn quite a lot about him through the song, which is also an indication of how good the song is. It’s full of wonderful lyrics, including some quotes by Kroc himself (*“competition, send em’ south / they’re gonna drown, put a hose in their mouth”* or *“it’s rat eat rat“*). That song kick started a long journey of me discovering his music, a journey that has made him, easily, one of my most cherished artists.

It’s hard to categorise his music. It isn’t conventional rock n’ roll. It isn’t conventional country, or blues, or folk rock. It’s sort of an “all of the above”. His music is definitely not of the head banging variety. He mumbles his lyrics, his interactions with the crowd can best be described as ‘polite’. And, once, when his throat gave out, he played his songs sitting down and in between sips of tea.

~~~
<center><iframe width="420" height="315" src="http://www.youtube.com/embed/L-9H-mt1vYg" frameborder="0" allowfullscreen></iframe></center>
~~~

None of this makes for ‘cool rockstar’ persona, but that is exactly why he is so fucking cool.

Knopfler is an artist whom people go to watch live just for the music, not the theatrics, visuals or the pyrotechnics. I had the good fortune of catching him live in Geneva in 2011. It was also the first concert that I ever saw in my life. I was so besotted by his music by then that the fact that he was touring with Bob Dylan may as well have been incidental for the 21-year-old me. That concert was exactly what I had hoped and imagined it would be. People tapping their feet, gently swaying, dancing on the spot, eyes half-closed, enjoying excellent music.

Music can do many things. It can help you deal with a [breakup](https://www.youtube.com/watch?v=ZBR2G-iI3-I). It sings the words of your heart when [you’re in love](https://www.youtube.com/watch?v=KLVq0IAzh1A). It can be [cerebral exercise](https://www.youtube.com/watch?v=iBfY86cktN0). It can help [vent your anger](https://www.youtube.com/watch?v=GoCOg8ZzUfg) (Sometimes, it can even be a [source of anger](https://www.youtube.com/watch?v=kffacxfA7G4)). It could be [sheer absurdity](https://www.youtube.com/watch?v=g4ouPGGLI6Q). At it’s most compelling though, it is therapy. Really, no therapy quite compares to music because it makes poetry of what you’re feeling. Mark Knopfler offers that therapy in spades. A big part of that is that he is one of the best storytellers in the world of music.

### Jazz for the layman

Despite the considerable respect he commands for his fingerstyle guitar playing, he prefers to think of himself as a songwriter first. And most of his songs tell a story. About someone, something, some place, or, sometimes, even some feeling.
[Devil Baby](https://www.youtube.com/watch?v=uJjlSnzaR8g) talks about our fascination with nature’s ‘marvels’ by using the example of ‘freak shows’ which used to be popular in the United States and in Europe. [This](https://thingssaidanddone.wordpress.com/2010/09/26/strange-and-bizarre-the-history-of-freak-shows/) is a great article on their history, but basically they were a gathering of malformed humans put on display for the viewing pleasure of the masses.

*See the pig faced man and the monkey girl\
Come see the big fat lady\
‘Gator Slim with the alligator skin\
Come see the Devil baby*

He mentions Jerry Springer (*Springer is a talker / he’s the talking man*) to make the point that we haven’t lost our fascination with the foibles and misfortunes of other people even in modern times (reality TV for instance). Indeed, he says, nowadays people welcome opportunities to showcase their weirdness.

*You can be on too\
With the nuts and the geeks\
Call 1-800-I’M A FREAK\
1-800-I’M A FREAK*

He ends with the lyrics *Come be the Devil baby*. Come, *be* the devil, reveal that dark, hidden side of yourself as you take pleasure in watching the unfortunates who will never have it as easy as you do.

[Get Lucky](https://www.youtube.com/watch?v=QtQZErwBNEw) is about this man who works seasonal blue-collar jobs and is happy if he can find two pennies to rub together.

*I’m better with my muscles\
Than I am with my mouth\
I'll work the fairgrounds in the summer\
Or go pick fruit down south*

*When I feel them chilly winds\
Where the weather goes, I’ll follow\
Pack up my travelling things\
Go with the swallows*

Knopfler’s soft, soothing voice is perfect for this song. In his matter-of-fact voice he says,

*I always think it’s funny,\
Gets me every time.\
The one about the happiness and money,\
Tell it to the breadline.*

As J.K. Rowling said in her Harvard commencement address, poverty itself is romanticised only by fools. Try convincing the people standing in line outside a soup kitchen that money doesn’t buy happiness.

The amazing thing, though, is that this is not an essay that he’s writing. It isn’t an exposition carefully explaining the nuances and intricacies of his viewpoint over 3000 words. It’s just a song! He manages to convey so much in a couple of verses and a chorus or two that you almost feel like you know the person.

And there are endless examples of such songwriting. In [Who’s Your Baby Now](https://www.youtube.com/watch?v=U4Fso196888), he is talking to a prostitute. This song was where I learned that prostitution is also called The Ancient Trade. The prostitute in question is a formerly-cocky, arrogant woman beaten into submission by life’s trials. She now sells her body to earn a living, but still can’t get by.

*You always had to be the kind\
To have to say what’s on your mind\
And hey, you really showed ’em how*

*You used to laugh about\
How you used to dish it out\
But hey, who’s laughing now*

You can almost see this woman’s past. You can visualise her cock-sure manner, her lack of mental filter as she breezed through life with her well-practiced half-sneer always on call. And yet, you can also see her present. That constant pit in her stomach, the anxiety about where she’ll get her next meal from. That bottomless abyss that greets you when you get to that point in life where the choices you make are coming home to roost.

There’s [Speedway at Nazareth](https://www.youtube.com/watch?v=QYtU1GfqjZ0) about a race car driver. I actually was part of a team that built race cars in college, and this song was a delight to discover. The song starts with the optimism that exists at the start of a new season.

*After two thousand came two thousand and one\
To be the new champions we were there for to run\
From springtime in Arizona ’til the fall in Monterrey\
And the raceways were the battlefields and we fought ’em all the way*

But, unfortunately, the season doesn’t go as planned and they constantly manage to fail one way or the other, leading to things like him having to drive “*Long Beach, California, with three cracked vertebrae*”, and just how cut-throat and all-consuming automobile racing can get:

*Well the brickyard’s there to crucify anyone who will not learn\
I climbed a mountain to qualify, went flat through the turns\
But I was down in the might-have beens, and an old pal good as died\
I sat down in gasoline alley and I cried*

The song ends on a positive note though, with them finally doing alright:

*New England, Ontario we died in the dirt\
Those walls from mid-Ohio to Toronto they hurt\
So we came to road America where we burned up the lake\
But at the speedway at Nazareth I made no mistake*

His voice trails off and almost becomes a whisper when he says "mistake" and you can sense that they *got* the success that they craved, so there's no need to shout from the rooftops about it, especially since it came at a considerable cost. But the tasteful guitar solo that goes on till the song ends to me signifies the reveling in the victory and just enjoying the moment without words.

Then there’s [Baloney Again](https://www.youtube.com/watch?v=UMzRjFxS4iQ) the lyrics for which, in their entirety (yeah, sorry, I couldn’t help myself), are here:

*We don’t eat in no white restaurants\
We’re eating in the car\
Baloney again, baloney again\
We don’t sleep in no white hotel bed\
We’re sleeping in the car, baloney again\
You don’t strut around in these country towns\
You best stay in the car\
Look on ahead don’t stare around\
You best stay where you are\
You’re a long way from home, boy\
Don’t push your luck too far\
Baloney again*

*Twenty-two years we’ve sung the word\
Since nineteen thirty-one\
Amen, I say amen\
Now the young folk want to praise the Lord\
With guitar, bass and drums, amen\
Well I’ll never get tired of Jesus\
But it’s been a heavy load\
Carrying His precious love\
Down a long dirt road\
We’re a long way from home\
Just let’s pay the man and go\
Baloney again*

*The Lord is my shepherd\
He leadeth me in pastures green\
He gave us this day\
Our daily bread and gasoline\
Go under the willow\
Park her up beside the stream\
Shoulders for pillows\
Lay down your head and dream\
Shoulders for pillows\
Lay down your head and dream*

The song is about this black gospel group, called the Fairfield Four, that used to tour in Tennessee back in the 50s. So, while they’re committed to spreading the Lord’s word, they are also constantly alert and wary of the white folk, looking to not cause any trouble. I adore the lyric “shoulder for pillows”. You can imagine those guys curling up and resting with their elbows tucked under their heads, because that’s as comfortable as it gets for them, and it just is what it is. I also love the invocation of "the Lord is my shepherd", because it somehow forms a continuous thread to our collective past.

Actually, he explains all this on Parkinson. You should check out the whole thing for yourself, just to realize how humble this man still is today.
~~~
<center><iframe width="420" height="315" src="http://www.youtube.com/embed/KpPYuGirBrA" frameborder="0" allowfullscreen></iframe></center>
~~~
Finally, one of my absolute favorites is [A Place Where We Used To Live](https://www.youtube.com/watch?v=GFnLXbcgmVw).

*Once there was a little girl\
Used to wonder what she would be\
Went out into the big wide world\
Now she’s just a memory\
There used to be a little school here\
Where I learned to write my name\
But time has been a little cruel here\
Time has no shame*

The song, to me, is about this couple who were childhood sweethearts, who’ve since broken up and moved on with their lives.

*Now in another town\
You lead another life\
And now upstairs and down\
You’re someone else’s wife\
Here in the dust\
There’s not a trace of us\
Everything is gone\
But my heart is hanging on*

It ends with a little guitar ditty that is only about 30 seconds long, but they're a really special 30 seconds that always tear me up.

This kind of songwriting just doesn’t come by very often. And this is one of the reasons why I’m thankful Mark never had his fill of the music world, but is still actively writing music and touring. And thankful that, thanks to people like him, music is alive and well even today.
