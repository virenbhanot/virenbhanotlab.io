@def title = "Isolated System"

# About
My name is Viren Bhanot and I'm a mechanical engineer at CERN. I completed my PhD at CERN in March, 2020 working on simulations of CO2-based cooling systems that will cool the next generation of Silicon detectors on the LHC experiments.

Previously, I got my master's at the University of Maryland, College Park, just outside DC. Beautiful place, and the master's was really enjoyable (and paid for!) I worked as a graduate research assistant doing dynamic simulations to investigate whether we could simply take out a high Global Warming Potential (GWP) from residential heat pumps and replace them with similar, lower GWP refrigerant without having to fundamentally change the hardware (The answer was, it depends.)

I've also worked in South India for the Indian automotive maker Mahindra and Mahindra, and before that, I studied in the second rainiest town in India called Manipal. Lots of 'M's in my life.

![](/assets/me.jpg)

My other interests include hiking, via ferratas, climbing and playing music (though I do none of those very well). I sometimes make [YouTube](https://www.youtube.com/c/VirenBhanot) videos about these things.

If you'd like to get in touch with me, drop me a line at hello at this website's name dot com.