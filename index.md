@def title = "Isolated System"

# Technical
[Conservation equations of fluid flow](/goveqns)\
[Different forms of the energy balance equation](/energybalance)\
[The problem of low mass flow rates](/lowmdot)\
[Modelling a control volume in Matlab](/matlabcv)\
[The 2PACL cooling systems at CERN](/2pacl)\
[On the relative growings of a ladder](/ladder)

# Personal
[Books I've read](/personal/readinglist)\
[Hikes I've been on](/personal/outside)\
[The master of the indecisive moment](/personal/indecisive)\
[Guitar george](/personal/guitargeorge)\
[Izzy Stradlin](/personal/izzy)\
[Advice for the self-conscious](/personal/selfconscious)\
[Quit things](/personal/quitting)\
[Things that irk](/personal/irk)\
[CERN](/personal/cern)\
[I use Arch, btw](/personal/arch)\
[How this blog is built](/personal/franklin)\
[About the writing style](/personal/writingstyle)\
[Hello, world!](/personal/helloworld)\
[About](/about)