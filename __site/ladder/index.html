<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/basic.css">
<link rel="stylesheet" href="/css/franklin.css">
<link rel="stylesheet" href="/libs/katex/katex.min.css">
<link rel="stylesheet" href="/libs/highlight/github.min.css">
<title>On the relative growings of a ladder</title> 
</head>
<body>
<header>
  <div class="blog-name"><a href="/">Isolated System</a></div>
</header><div class="franklin-content"><h1 id="on_the_relative_growings_of_a_ladder"><a href="#on_the_relative_growings_of_a_ladder" class="header-anchor">On the relative growings of a ladder</a></h1>
<p>2025-01-03</p>
<p>Thompson&#39;s 1965 masterpiece &quot;Calculus Made Easy&quot; is one of the best, most enjoyable textbooks that I&#39;ve ever read.</p>
<blockquote>
<p>&quot;Being myself a remarkably stupid fellow, I have had to unteach myself the difficulties, and now beg to present to my fellow fools the parts that are not hard&quot;.</p>
</blockquote>
<p>A classic.</p>
<p>In Chapter 2, he talks about a ladder resting against a wall. As we pull the ladder away from the wall, it descends down the wall. He makes an offhand remark that as the ladder is pulled away from the wall, &quot;the height \(y\) reached by the ladder decreases in a corresponding manner, <em>slowly at first, but more rapidly as x becomes greater</em>.&quot; &#91;emphasis mine&#93;</p>
<p>What a wonderful journey to go down on.</p>
<p>The calculations are straightforward. The length of the ladder is fixed, and x &#40;the horizontal distance to the wall&#41; and y &#40;the height reached by the ladder&#41; are related by the Pythagoras Theorem:</p>
\[x^2 + y^2 = L^2\]
<p>L being the length of the ladder of course.</p>
<p>So, what happens when we pull the ladder outward? x becomes x &#43; dx, and y becomes y &#43; dy, but the length of the ladder itself remains unchanged.</p>
\[(x+dx)^2 + (y+dy)^2 = L^2\]
<p>This new length must be the same as the old length. Therefore,</p>
\[(x+dx)^2 + (y+dy)^2 = x^2 + y^2\]
\[x^2+2xdx+(dx)^2 + y^2+2ydy+(dy)^2 = x^2 + y2\]
<p>Now, we neglect \((dx)^2\) and \((dy)^2\) because of Chapter 2: On different degrees of smallness. Also, \(x^2\) and \(y^2\) cancel out, as does the &#39;2&#39;.</p>
\[x.dx + y.dy = 0\]
<p>Leaving,</p>
\[dy/dx = -x/y\]
<p>Easy enough to calculate<sup id="fnref:1"><a href="#fndef:1" class="fnref">[1]</a></sup>.</p>
<p>We can also check this with a real example. 5 m ladder, 4 m up the wall. Of course, that means it is 3 m away from the wall. Now, if we slide it by 0.1 cm:</p>
\[(y+dy)^2 = 5^2 - 3.1^2\]
\[(y+dy)^2 = 15.39\]
\[(y+dy) = 3.923\]
\[dy = 3.923-4 = -0.0769\]
<p>Which is very close to \(- x.dx/y = -0.075\). It would have been closer still had we not taken such a huge dx of 0.1 m.</p>
<p>Great, so far so trivial. But that throwaway remark kept eating at me. How have I lived 35 years without knowing that the ladder didn&#39;t somehow slide <em>linearly</em> down the wall?</p>
<p>Let&#39;s plot this.</p>
<p><img src="/assets/ladder.png" alt="" /></p>
<p>Well, I&#39;ll be damned. </p>
<p>When the ladder is nigh-on vertical against the wall &#40;x &#61; 0.01&#41;, the height along the wall is virtually the entire length of the ladder. Then, as we start pulling it, almost nothing happens at first. After we&#39;ve pulled the ladder a whole metre away, we&#39;re still only 0.1 metres down the wall. Heck, after we&#39;re well past halfway pulling the ladder apart, at x &#61; 3 m, we&#39;re only just beginning to lose the first metre of height &#40;y &#61; 4 m&#41;. The full results are below:</p>
<table><tr><th align="right">x</th><th align="right">y</th></tr><tr><td align="right">0.01</td><td align="right">5.00</td></tr><tr><td align="right">0.50</td><td align="right">4.97</td></tr><tr><td align="right">1.00</td><td align="right">4.90</td></tr><tr><td align="right">2.00</td><td align="right">4.58</td></tr><tr><td align="right">3.00</td><td align="right">4.00</td></tr><tr><td align="right">4.00</td><td align="right">3.00</td></tr><tr><td align="right">4.95</td><td align="right">0.70</td></tr></table>
<p>Remarkable. Now, dy/dx depends on x. So, the larger the x becomes, the larger the rate of change becomes. But it also depends on y, so the <em>smaller</em> y becomes, the larger still becomes the rate of change. Double whammy&#33; Below is the plot of dy/dx plotted against x.</p>
<p><img src="/assets/ladder2.png" alt="" /></p>
<p>The effect is much more dramatic than I imagined. Even going from three metres to four metres away from the wall, we still only lost one more metre in height. But in the last 0.95 metres, we lose nearly all of our length. This is confirmed by the dy/dx plot, with the rate of change skyrocketing as we careen towards the end.</p>
<p>What was the point of this post? Nothing. Just an interesting thought exercise.</p>
<p>Here&#39;s the code for the plots. I generated it using Octave, because I can&#39;t figure out how to run Matlab on Wayland.</p>
<pre><code class="language-julia">clc
clear

L &#61; 5;

x &#61; &#91;0.01 0.5 1 2 3 4 4.95&#93;;
y &#61; sqrt&#40;L.^2 - x.^2&#41;;
dydx &#61; -x./y 

set&#40;0,&#39;defaultlinelinewidth&#39;,2&#41;;
set&#40;0,&#39;defaulttextfontname&#39;,&#39;Cooper Hewitt Medium&#39;&#41;;
set&#40;0,&#39;defaultaxesfontname&#39;,&#39;Cooper Hewitt Medium&#39;&#41;;
set&#40;0,&#39;DefaulttextFontSize&#39;,13&#41;;
set&#40;0,&#39;DefaultaxesFontSize&#39;,13&#41;;

aoeu &#61; figure;
set&#40;gcf,&#39;Units&#39;,&#39;centimeters&#39;,&#39;Position&#39;,&#91;1, 1, 30, 20&#93;&#41;;

subplot&#40;2,1,1&#41;;
h &#61; plot&#40;x,y,&#39;-o&#39;&#41;;
xlabel&#40;&#39;Distance from wall &#40;x&#41;&#39;&#41;;
ylabel&#40;&#39;Height along wall &#40;y&#41;&#39;&#41;;
xlim&#40;&#91;0, 5&#93;&#41;;
ylim&#40;&#91;0, 5&#93;&#41;;
grid on

subplot&#40;2,1,2&#41;
hold on
for i &#61; 1:length&#40;x&#41;
	str &#61; sprintf&#40;&#39;x &#61; &#37;d&#39;,x&#40;i&#41;&#41;;
	plot&#40;&#91;x&#40;i&#41;,0&#93;,&#91;0,y&#40;i&#41;&#93;,&#39;DisplayName&#39;,str&#41;;
end
legend

figure
plot&#40;x,dydx&#41;
xlabel&#40;&#39;Distance from wall &#40;x&#41;&#39;&#41;;
ylabel&#40;&#39;Rate of change &#40;dy/dx&#41;&#39;&#41;;
grid on
box on</code></pre>
<table class="fndef" id="fndef:1">
    <tr>
        <td class="fndef-backref"><a href="#fnref:1">[1]</a></td>
        <td class="fndef-content">In the book Thompson includes a negative sign <code>x&#43;dx ⇒ y-dy</code>, but then, at the last minute, pulls the rug from under us and corrects himself, giving dy/dx &#61; -x/y. But, if he&#39;d assumed y-dy, he should have gotten &#43;x/y.</td>
    </tr>
</table>

</div><!-- CONTENT ENDS HERE -->
    
        <script src="/libs/katex/katex.min.js"></script>
<script src="/libs/katex/auto-render.min.js"></script>
<script>renderMathInElement(document.body)</script>

    
    
        <script src="/libs/highlight/highlight.pack.js"></script>
<script>hljs.initHighlightingOnLoad();hljs.configure({tabReplace: '    '});</script>

    
  </body>
</html>
