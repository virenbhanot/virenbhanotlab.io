<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/basic.css">
<link rel="stylesheet" href="/css/franklin.css">
<link rel="stylesheet" href="/libs/katex/katex.min.css">
<link rel="stylesheet" href="/libs/highlight/github.min.css">
<title>The Energy Balance Equation</title> 
</head>
<body>
<header>
  <div class="blog-name"><a href="/">Isolated System</a></div>
</header><div class="franklin-content"><h1 id="different_forms_of_energy_balance_equation"><a href="#different_forms_of_energy_balance_equation" class="header-anchor">Different forms of energy balance equation</a></h1>
<p>The energy balance equation is more complicated, in appearance at least, than the mass balance equation. As one navigates the field of thermofluids, one comes across this equation in several forms. In this article, I want to show you the derivations of these different forms, arriving ultimately to forms that can be implemented into computer code.</p>
<p>To start with, the differential form of the energy balance equation is as follows:</p>
\[\frac {\partial(\rho u V)} {\partial t} = \Sigma \dot mh\]
<p>With this equation, we will play around quite a bit. Note that, for simplicity, I didn&#39;t add any heat source or transfer terms \(\dot Q\) on the right hand side, but these could be added as needed. For instance, you might have heat transfer to a wall like \(Q = \alpha A(T_{wall}-T_{ref})\). Note also that, for this post, I will write only about rigid control volumes, i.e. the shape doesn&#39;t deform, because there is plenty to discuss just within that constraint. </p>
<p>We will also use the following two things:</p>
<ul>
<li><p>The mass balance equation \(\frac {\partial(\rho V)} {\partial t} = \Sigma \dot m\)</p>
</li>
<li><p>The fact that enthalpy, \(h = u + Pv\)</p>
</li>
</ul>
<h2 id="first_form"><a href="#first_form" class="header-anchor">First form</a></h2>
<p>First, let me just state that &quot;first form&quot; isn&#39;t an official term. It is just that I&#39;m writing this form up first. To start with, we write the enthalpy definition as \(u=h-p/\rho\). This is the same as the second bullet point above, because the volume in that equation is the <em>specific volume</em>, aka the reciprocal of density. </p>
<p>Now, on substituting this into Equation &#40;1&#41; we get:</p>
\[\frac{\partial(\rho(h-P/\rho)V)}{\partial t} = \Sigma\dot mh\]
\[\frac{\partial((\rho h-P)V)}{\partial t} = \Sigma\dot mh\]
\[V\left(\frac{\partial(\rho h)}{\partial t} - \frac{dP}{dt}\right)= \Sigma\dot mh\]
<p>We take volume out of the derivative because it doesn&#39;t change with time &#40;and so it&#39;s rate of change with time would be zero&#41;. Also, we replace the partial derivative \(\partial\) with the derivative \(d\) for pressure. This is because pressure and specific enthalpy are our state variables, aka our independent parameters. Thus, they are functions only of time and not of other thermodynamic parameters.</p>
<p>Two things now. First, we use the product rule<sup id="fnref:1"><a href="#fndef:1" class="fnref">[1]</a></sup> on the \(\rho h\) derivative term. Second, we remember that, in thermodynamics, if we know two independent properties, we can determine all other parameters in terms of them. In effect, that is, every property can be expressed <em>as a function of</em> the two independent properties. So, for our purposes, we use this to write \(\rho = f(P,h)\). So that: </p>
\[\frac {\partial \rho}{\partial t} = \frac {\partial \rho}{\partial P} \frac {dP}{dt}\Bigg|_h\ +  \frac {\partial \rho}{\partial h} \frac {dh}{dt}\Bigg|_P\ \]
<p>Where, the derivatives \(\frac{dP}{dt}\) and \(\frac{dh}{dt}\) are calculated holding the other property constant.</p>
<p>Then, we can do the following with the first derivative term:</p>
\[\frac {\partial(\rho h)} {\partial t} = h\frac {\partial \rho}{\partial t} + \rho\frac {dh}{dt} = h\left(\frac {\partial \rho}{\partial P} \frac {dP}{dt} +  \frac {\partial \rho}{\partial h} \frac {dh}{dt}\right)+\rho\frac {dh}{dt}\]
<p>Substituting this final term into Equation &#40;4&#41; we get</p>
\[V\left(h\left(\frac {\partial \rho}{\partial P} \frac {dP}{dt} +  \frac {\partial \rho}{\partial h} \frac {dh}{dt}\right)+\rho\frac {dh}{dt} - \frac{dP}{dt}\right)= \Sigma\dot mh\]
<p>Taking the derivatives common, then:</p>
\[V\left(\left(h\frac {\partial \rho}{\partial P}-1\right) \frac {dP}{dt} + \left(h\frac {\partial \rho}{\partial P}+\rho\right) \frac {dh}{dt} \right)  = \Sigma\dot mh\]
<p>This form is perhaps familiar to you, since it shows up in many places.</p>
<h2 id="second_form"><a href="#second_form" class="header-anchor">Second Form</a></h2>
<p>Now, let&#39;s go back to our original form and add a different flavour to the derivation. First, instead of substituting for \(u\) right away, we instead do the following:</p>
\[u\frac{\partial(\rho V)}{\partial t} + \rho V\frac{\partial u}{\partial t} = \Sigma \dot mh_{bdry}\]
<p>That is, we use the product rule <sup id="fnref:1"><a href="#fndef:1" class="fnref">[1]</a></sup> to split it into a \(\rho V\) part and a \(u\) part. Note the \(bdry\) subscript I added to the last term this time. This is to emphasise that the enthalpies in the boundary terms may not be the same as the enthalpy of the control volume. Instead, the enthalpies here will be the upstream enthalpy.</p>
<p>Now, maybe you see the next step already, but the first differential term on the left hand side is also the LHS of the mass balance equation \(\frac{\partial(\rho V)}{\partial t}\). We can substitute that in:</p>
\[u \Sigma\dot m + \rho V\frac{\partial u}{\partial t} = \Sigma \dot mh\]
<p>And now can add our term for internal energy in terms of enthalpy:</p>
\[\left(h-P/\rho\right)\Sigma\dot m  + \rho V \frac{\partial (h-Pv)}{\partial t} = \Sigma\dot mh_{bdry}\]
<p>Then, we move some stuff around to get:</p>
\[\rho V \frac{\partial (h-P/\rho)}{\partial t} = \Sigma\dot m\left(h_{bdry}-h+P/\rho\right)\]
<p>Now let&#39;s simplify the left hand side differential term. We will use the quotient rule<sup id="fnref:2"><a href="#fndef:2" class="fnref">[2]</a></sup> here.</p>
\[\rho V\left(\frac{\partial (h-P/\rho)}{\partial t}\right) = \rho V\left(\frac{dh}{dt} - \frac{\partial (P/\rho)}{\partial t} \right) = \rho V\left(\frac{dh}{dt} - \left(\frac{\rho\frac{dP}{dt} -P\frac{\partial\rho}{\partial t}}{\rho^2}\right)\right) = V\left(\rho\frac{dh}{dt}-\frac{dP}{dt}+\frac{P}{\rho}\frac{\partial\rho}{\partial t}\right)\]
<p>Ok, things start to look a bit more manageable. The final thing to do is to substitute the terms for the partial derivative of density in terms of pressure and enthalpy, as in the equation above.</p>
\[\frac{P}{\rho}\frac{\partial\rho}{\partial t} =  \frac{P}{\rho}\left(\frac {\partial \rho}{\partial P} \frac {dP}{dt} +  \frac {\partial \rho}{\partial h} \frac {dh}{dt}\right)\]
<p>I got rid of the &#39;constant pressure/enthalpy&#39; symbols for simplicity.</p>
<p>OK. So the full energy balance equation is:</p>
\[ V\left(\left(\frac{P}{\rho}\frac{\partial\rho}{\partial h}+\rho\right)\frac{dh}{dt} + \left(\frac{P}{\rho}\frac{\partial\rho}{\partial P}-1\right)\frac{dP}{dt}\right) = \Sigma\dot m\left(h_{bdry}-h+P/\rho\right)\]
<p>Done.</p>
<p>This form <em>looks</em> considerably more complicated than the previous one, but the equations are the same. It&#39;s just a different way to derive. Now, remember that \(h_{bdry}\) is derived from the upwind scheme. This means that, for any flow going OUT of the control volume under consideration, \(h_{bdry} = h\). Then, these terms will cancel out, leaving only \(\dot m_{out}\frac{P}{\rho}\) for all outlet flows.</p>
<p>Another thing to note is that all these \(P/\rho\) stuff only applies to the &quot;flow&quot; things. The source terms \(Q\) are left alone.</p>
<h2 id="third_form"><a href="#third_form" class="header-anchor">Third Form</a></h2>
<p>This one&#39;s slightly different.</p>
<p>So far, we&#39;ve been considering the energy of the control volumes as \(\rho u V\). But this is just a fancy way of saying \(U\), because ultimately, that is what the energy balance equation deals with: that the total energy of the control volume.</p>
<p>In most cases, we can assume that the density \(\rho\) is constant within the control volume, and thus the bulk \(\rho uV = U\) can be taken without issue.</p>
<p>Then, the energy balance equation is written as:</p>
\[\frac{\partial U}{\partial t} = \Sigma\dot mh_{bdry}\]
<p>Now, \(U = H - PV\). Here, we are talking extensive properties rather than specific properties &#40;aka per unit mass&#41;. The Volume, therefore, is not specific volume, but rather, the volume of the control volume. If we stick this into our equation:</p>
\[\frac{dH}{dt}-\frac{\partial (PV)}{\partial t} = \Sigma\dot mh \]
<p>But, Volume doesn&#39;t change with time for a rigid control volume. Therefore:</p>
\[\frac{dH}{dt}-V\frac{dP}{dt} = \Sigma\dot mh_{bdry}\]
<p>Now, \(H = M*h\). Substituting this and using the product rule:</p>
\[M\frac{dh}{dt}  h\frac{dM}{dt} - V\frac{dP}{dt} = \Sigma\dot mh_{bdry}\]
<p>And, \(M = \rho V\). So that</p>
\[\rho V\frac{dh}{dt}-h\frac{dM}{dt}-V\frac{dP}{dt} = \Sigma\dot mh_{bdry}\]
<p>Finally, \(dM/dt = \Sigma\dot m\) is the mass balance equation. So, if we substitute it in and transport that term to the right hand side, we get</p>
\[\rho V\frac{dh}{dt}-V\frac{dP}{dt} = \Sigma\dot m(h_{bdry}-h)\]
<p>Nice. This form is especially enjoyable because of it&#39;s simplicity. No partial derivatives are necessary, and the format of the equation is very simple. Note, however, that no partial derivatives are necessary <em>for the energy balance equation</em>. You will still need them for the mass balance calculations.</p>
<h2 id="implementation"><a href="#implementation" class="header-anchor">Implementation</a></h2>
<p>Now, let&#39;s implement them into Matlab and do some verifications shall we? I will take the same example I took <a href="/matlabcv">last time</a> of equal mass flow rate into and out of the control volume. Then, the problem becomes isochoric &#40;constant density&#41; because the total mass inside the control volume never changes.</p>
<p>I will call these forms F1, F2 and F3 respectively. I&#39;ve talked about the code for F1, I&#39;ll skip the code for F2 &#40;and you can take my word for it&#41;. Instead, only the F3 code is given below:</p>
<pre><code class="language-julia">function y &#61; controlVolumeF3&#40;~,x&#41;
&#37; controlVolumeF3: Third form of control volume.
&#37; Simplified form described by Richter &#40;2008&#41; and Pini &#40;2013&#41;
&#37; DOES NOT SUPPORT REVERSE-FLOW YET

global fld Vol mDotIn hin mDotOut Qh Qc

P &#61; x&#40;1&#41;;
h &#61; x&#40;2&#41;;

&#91;drhodPh,drhodhP&#93; &#61; getPartialDers&#40;P,h,fld&#41;;
rho &#61; refpropm&#40;&#39;D&#39;,&#39;P&#39;,P*1e-3,&#39;H&#39;,h,fld&#41;;

M &#61; &#91;Vol*drhodPh, Vol*drhodhP;...
	-Vol, rho*Vol&#93;;
f &#61; &#91;mDotIn-mDotOut ; mDotIn*&#40;hin-h&#41; &#43; Qh - Qc&#93;;

y &#61; M\f;
end</code></pre>
<p><code>getPartialDers</code> is a function I wrote to calculate the partial derivatives properly in every region, including the two-phase region. It&#39;s a &#40;very interesting&#41; topic for another day. The comment screaming that reverse flow isn&#39;t supported is saying that here, flow comes in from the inlet port and goes out from the outlet port. There is no possibility of flow coming <em>in</em> from the outlet port. This would require a more rigorous upwind scheme implementation, which I&#39;m too lazy to do.</p>
<p>The comparison between the forms is given below:</p>
<p><img src="/assets/energybalance_1.png" alt="" /></p>
<p>Thus, the results stay the same. Hardly a surprise, of course, considering all we&#39;ve done is manipulated the maths around without changing the underlying assumption about the physics. But yeah, we&#39;ve now got some horses for courses. </p>
<p>To be honest, it&#39;s hard for me to say why we implement these different forms. I guess it&#39;s just an artefact of history that different people derived it different ways when they were dealing with the maths. So far as I can tell, you are free to use either form without hindrance.</p>
<p><table class="fndef" id="fndef:1">
    <tr>
        <td class="fndef-backref"><a href="#fnref:1">[1]</a></td>
        <td class="fndef-content">Product rule: if \(f(x) = u(x).v(x)\), then \(f'(x) = u'(x).v(x)+u(x)v'(x)\)</td>
    </tr>
</table>
<table class="fndef" id="fndef:2">
    <tr>
        <td class="fndef-backref"><a href="#fnref:2">[2]</a></td>
        <td class="fndef-content">Quotient rule: if \(f(x) = u(x)/v(x)\), then \(f'(x) = \frac{u'(x)v(x)-u(x)v'(x)}{v(x)^2}\)</td>
    </tr>
</table>
</p>
</div><!-- CONTENT ENDS HERE -->
    
        <script src="/libs/katex/katex.min.js"></script>
<script src="/libs/katex/auto-render.min.js"></script>
<script>renderMathInElement(document.body)</script>

    
    
        <script src="/libs/highlight/highlight.pack.js"></script>
<script>hljs.initHighlightingOnLoad();hljs.configure({tabReplace: '    '});</script>

    
  </body>
</html>
