<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/basic.css">
<link rel="stylesheet" href="/css/franklin.css">
<link rel="stylesheet" href="/libs/katex/katex.min.css">

<title>Governing equations of fluid flow</title> 
</head>
<body>
<header>
  <div class="blog-name"><a href="/">Isolated System</a></div>
</header><div class="franklin-content"><h1 id="conservation_equations_of_fluid_flow"><a href="#conservation_equations_of_fluid_flow" class="header-anchor">Conservation equations of fluid flow</a></h1>
<p><em>This article discusses the absolute fundamental equations that dictate how things flow in pipes, and therefore, form the basis of all of my modelling</em></p>
<h2 id="houskeeping"><a href="#houskeeping" class="header-anchor">Houskeeping</a></h2>
<p>Before discussing any form of mathematical modelling, we must first understand that we need as many equations as there are unknowns. If there are 5 &#39;things&#39; unknown about a system, then we must have 5 equations. Any more and we over-constrain the system, and so not all of the equations &#40;potentially&#41; can be satisfied. Any fewer and we are under-constrained, that is, there aren&#39;t enough equations to find a unique value for each variable.</p>
<p>Note that I am not saying that if you have as many equations as variables, you will <em>certainly</em> find a solution. Just that, without the same number, you aren&#39;t guaranteed a good answer. Necessary but not sufficient.</p>
<p>We begin with the concept of &#39;Control Volume&#39;. This is the fundamental unit of analysis in the field of thermofluids. A control volume is nothing more than a region of space, usually of finite volume &#40;as opposed to an infinitesimal or infinite region&#41; that we are interested in. It might be the inside of a water pipe, for example, or a sliver of air just above the outer edge of an aircraft wing. We analyse such volumes in the same way as when we draw a Free-Body Diagram in structural analysis; we specify and then balance the different elements flowing into or out of the region. A control volume has a boundary that we call a control surface, which may or may not be a real, tangible thing.</p>
<p>Now, in the most general case, the control volume would be a three-dimensional object. Often, however, the conduits we use are symmetric around an axis, and we can assume that the major changes to a fluid property happen along the direction of flow, and the change along the transverse direction are much smaller. This way, we can average the properties at each cross section, leading to a one-dimensional flow assumption. We could even go a step further and just assume the whole conduit is one property, leading to the &#39;Lumped&#39; assumption. In most of my work, I use a 1D assumption, because it is good enough, while still being simple enough to let me run complex systems.</p>
<p>We next need to understand that, in thermodynamics, when we are dealing with a pure or pseudo-pure substance, if we know the values of two independent properties of the substance, we can calculate all the other properties. That is, if we know pressure and density, for example, we can now calculate the enthalpy, internal energy, the temperature, the viscosity, the surface tension and all the rest of it. Similarly, if we know pressure and enthalpy, or density and internal energy, we can calculate the other stuff. If we are calculating saturation properties, then just one property is enough. So, for a given pressure of a liquid, there is a unique saturation temperature, a unique density etc. </p>
<p>This, however, is not the full story. Knowing the thermodynamic state of a fluid does not give us any fluid dynamic information. In other words, we are still missing the &#39;flow&#39; properties. In other words still, we are missing knowledge of the flow rate of the substance. This might be in the form of velocity &#40;m/s&#41;, the volumetric flow rate &#40;L/s&#41; or mass flow rate &#40;kg/s&#41;.</p>
<p>So what we have now are three unknowns. Conversely, if we can figure out these three unknowns, we can figure out everything about the control volume in question. Thus, we are looking for three equations to do the trick.</p>
<h2 id="reynolds_transport_theorem"><a href="#reynolds_transport_theorem" class="header-anchor">Reynold&#39;s transport theorem</a></h2>
<p>I want to highlight a feature of control volumes that may not be immediately obvious from the past few paragraphs. <em>A control volume does not have to be stationary</em>. It is not essential that the control volume stay in place while fluid flows in and out of it. We could have a control volume that flows at precisely the speed of the fluid, and thus, it is as if we have chosen our favourite bit of the fluid, and are following it around as it flows through the system, and seeing what happens to it. </p>
<p>In fact, this method of analysis has a name for it. It&#39;s called the <em>Lagrangian</em> approach. The alternative, where the control volume is fixed and fluid flows in and out, is called the <em>Eulerian</em> approach.</p>
<p>Neither of these is better or worse than the other, but instead are suited for different tasks. More importantly, we helpfully have a way to convert between the two approaches using an identity called the Reynold&#39;s Transport Theorem.</p>
<p>If we have an Extensive property<sup id="fnref:1"><a href="#fndef:1" class="fnref">[1]</a></sup> Y, then the intensive version of the property is \(\frac{dy}{dM}\). The Reynold&#39;s transport theorem states that</p>
\[\frac{dY_{system}}{dt} = \frac{\partial}{\partial t}\left( \iiint_{CV} y\rho dV\right) + \iint_{CS} y.\rho \left( \vec{v}.d\vec{S}\right)\]
<p>This is a rather complex looking equation, and deriving it is also non trivial, but understanding the intuition of it is rather simple.</p>
<p>Let&#39;s say you have a system with fluid flowing in a closed loop. Now, the left side of the equation measures the change over time of any extensive property &#40;let&#39;s say system energy&#41;. Then, this equation is merely stating that any change in the total energy of the system &#40;the left hand side&#41; can be caused either by a change in the internal energy of the system &#40;say you had a lots of drag along a wall generating heat&#41;, or energy crossing the &#39;control surface&#39; &#40;say a heater heating up the fluid&#41;.</p>
<p>That&#39;s it. That&#39;s all the governing equations say. That we cannot have phantom generation or destruction of conserved quantities such as mass or energy.</p>
<p>The left hand side in Equation 1 above is the Lagrangian approach, while the right hand side is the Eulerian approach. The Reynold&#39;s transport theorem gives us a way to relate the two.</p>
<p>The three governing equations that we deal with basically take the form of the equation above, where we change &#39;Y&#39; to the correct property. Let&#39;s look at them one by one.</p>
<h2 id="conservation_of_mass"><a href="#conservation_of_mass" class="header-anchor">Conservation of mass</a></h2>
<p>This equation states that mass can neither be created nor destroyed. Although CERN does destroy mass often, this is not a problem on the scale of thermodynamics that we are dealing with. For our purposes, mass is a fixed thing.</p>
<p>To find the format of the equation, we substitute Y as mass &#40;M&#41; in the equation above. But, mass is fixed, therefore, \(\frac{dM}{dt}=0\). Also, y &#40;aka &quot;mass per unit mass&quot;&#41; is just 1. Then, the governing equation of mass is:</p>
\[\frac{\partial}{\partial t}\left( \iiint_{CV} \rho dV\right) + \iint_{CS} \rho \left( \vec{v}.d\vec{S}\right) = 0\]
<p>I want to stop and point out one thing here. Those integral signs. Let&#39;s say we have a cube. Now, instead of assuming that the fluid properties &#40;say density&#41; is constant in the whole region, we could chop up the cube into smaller segments, and then sum up the properties for each segment. For example, say we divide the cube into 3 pieces along each side. This gives us 27 smaller cubes. Calculating the average properties of 27 small cubes gives us a better approximation than 1 large cube. By making these 27 cubes into 64, 125, 216, 343 etc., we can get better and better approximations. The integral sign just indicates the idea of infinitely many &#39;discretizations&#39;.</p>
<p><img src="/assets/goveqns_1.png" alt="" /></p>
<p>Now, we have the intuition down for the equation, but in the current format, it&#39;s not easy to implement, so let&#39;s alter that. Let&#39;s first assume that our pipe is circular. This isn&#39;t required, it just makes things easier for me to talk about. Now, I make the assumption that I talked about earlier; that the flow properties mainly change along the direction of flow. This allows me to assume axisymmetry, that is, we average the properties at across the cross-section.</p>
<p><img src="/assets/goveqns_2.png" alt="" /></p>
<p>Now, we make some simplifications. The Control Surface we were talking about is now just the inlet and the outlet port because those are the only two places where flow can come in or out. Then, the \(\iint_{CS} \rho \left( \vec{v}.d\vec{S}\right)\) becomes \(\dot{m}_{in}-\dot{m}_{out}\), because the flow can&#39;t come in through the surface.</p>
<p>Next, about that triple &#40;volume&#41; integral \(\frac{\partial}{\partial t}\left( \iiint_{CV} \rho dV\right)\), two of those dimensions cease to be of interest to us, because of the aforementioned axisymmetry. Therefore, this becomes \(\frac{\partial}{\partial t}\left(A. \int_{x} \rho dx\right)\).</p>
<p>Then, our simplified equation now looks like:</p>
\[\frac{\partial}{\partial t}\left(A. \int_{x} \rho dx\right) + \dot{m}_{in}-\dot{m}_{out} = 0\]
<p>Excellent.</p>
<h2 id="momentum_equation"><a href="#momentum_equation" class="header-anchor">Momentum equation</a></h2>
<p>The most general form of the momentum equation is called the Navier-Stokes equation. Or, at least, that is what we were taught<sup id="fnref:1"><a href="#fndef:1" class="fnref">[1]</a></sup>. It seems that it is more proper to collectively call <em>all</em> of the governing equations dictating viscous flow the Navier-Stokes equations. These equations hold a mystical place in the annals of Physics, what with them being one of the Millennium Prize Problems. But our purposes here are a lot more utilitarian.</p>
<p>The momentum equation starts out life as Newton&#39;s Second Law of Motion, aka \(Force = Mass \times Acceleration\). If taken instantaneously, this equation can be thought of as: an instantaneous change in Force causes an instantaneous change in the product of mass and acceleration.</p>
<p>Let&#39;s look at the right hand side of the equation first. Acceleration is the rate of change of velocity over time, aka \(d\vec{v}/dt\). Therefore, the right hand side of the equation is then the rate of change of &#40;Mass * Velocity&#41;, aka the rate of change of momentum. </p>
<p>Effectively, if we flip the way we think of the equation around, we might say that <em>if there is a net force on the control volume, it will cause a change in the momentum of the fluid</em>.</p>
\[\frac{D}{Dt}\left(m\vec{v}\right)=\Sigma\vec{F}\]
<p>Now, if we take our first equation and substitute momentum, aka \(m\vec{v}\) for our do-it-all variable &#39;Y&#39;, we get</p>
\[ \frac{D}{Dt} \left(m\vec{v}\right) = \frac{\partial}{\partial t}\left( \iiint_{CV} \vec{v}\rho dV\right) + \iint_{CS} \vec{v}\rho \left( \vec{v}.d\vec{S}\right) \]
<p>Again, the left hand side is the Lagrangian expression, aka if we were following a bit of fluid around and examining it&#39;s momentum change, while the right hand side is the Eulerian expression.</p>
<p>But, our momentum change is the net force, so that the equation of momentum will read: </p>
\[ \Sigma \vec{F} = \frac{\partial}{\partial t}\left( \iiint_{CV} \vec{v}\rho dV\right) + \iint_{CS} \vec{v}\rho \left( \vec{v}.d\vec{S}\right) \]
<p>We could do a similar simplification that we did for the mass balance equation. I won&#39;t do it here, mainly because it won&#39;t help in any further gaining of intuition. Instead, let&#39;s look at our final equation.</p>
<h2 id="conservation_of_energy"><a href="#conservation_of_energy" class="header-anchor">Conservation of energy</a></h2>
<p>We know the method by now. Take our first equation, and substitute the variable of interest. In this case, no prizes for guessing, we substitute energy for our variable &#39;Y&#39;.</p>
\[\frac{D}{Dt}\left(E\right) = \frac{\partial}{\partial t}\left( \iiint_{CV} e\rho dV\right) + \iint_{CS} e\rho \left( \vec{v}.d\vec{S}\right) \]
<p>This \(e\) here is the <em>total</em> energy of the fluid. The total energy is the sum of the internal energy &#40;the jiggling energy of the molecules of fluid&#41;, plus the kinetic and potential energies.</p>
<p>Anyway, we now look at the first law of thermodynamics, which says that for a system, the change in energy is caused by the sum of any heat input/output from the system, plus any work done by or on the system.</p>
\[\frac{D}{Dt}\left(E\right)=\Sigma\dot{Q} -\Sigma\dot{W} \]
<p>The work component in the equation above is interesting, because work comes in many forms. First, what is work? It is Force times the displacement. Work can come in many forms. Things could move under a magnetic field, under an electric field. A shaft could stir things up, causing movement etc. For our purposes, we will neglect just about all these kinds of work. Our only concern is this thing called the pressure work.</p>
<h3 id="pressure-volume_work"><a href="#pressure-volume_work" class="header-anchor">Pressure-volume work</a></h3>
<p>An object at rest continues to be at rest until acted upon by an external force. This statement is familiar to us. Now, let&#39;s take our pipe from before. As you see in the picture below, Johnny-come-lately Mr. Diagonal Hatching particle wants to enter the grey pipe. But the particles already in the pipe are resistant to move. Thus, to be able to enter the pipe, it needs to displace a region equivalent to it&#39;s own volume, which is shown as the cross hatching.</p>
<p><img src="/assets/goveqns_3.png" alt="" /></p>
<p>So, what is the work done by this volume? Well, again, work is force times displacement. Now, we know that pressure is force per unit area. Thus, force is pressure times the area. Also, the displacement here is the actual &#39;length&#39; of this particle &#40;dL in the figure&#41;.</p>
<p>Then, the work done is \(W_{PV}=P.A.dL = PV\)</p>
<p>You see this displaced volume as the Cross Hatching volume at the outlet. This is the origin of the pressure work, and this is the only one we will consider in this article. The figure above only considered the possibility of flow going into and out of the pipe via the inlet ports. But we could generalise this to a deformable control volume as well, where any deformation of the control surface would need some work done on it. For the moment, I consider a rigid volume, so let&#39;s forget about the deformability. Also, note that the equation above is work done, but the <em>rate</em> of work done &#40;in the units of Power&#41;, would be \(P\dot{V}\), aka the rate of change of the volume.</p>
<p>Right, back to regularly scheduled programming. We now substitute our PV work and our heat transfer into the energy balance equation.</p>
\[ \Sigma\dot{Q}-P\dot{V} = \frac{\partial}{\partial t}\left( \iiint_{CV} e\rho dV\right) + \iint_{CS} e\rho \left( \vec{v}.d\vec{S}\right) \]
<p>The kinetic and potential energy portions are sometimes important &#40;especially when there are large height differences&#41; but for simplicity, I will neglect them for now. Thus, \(e\) can be replaced by \(u\).</p>
<p>Then, a simplified energy balance equation will look like:</p>
\[ \Sigma\dot{Q}-P\dot{V} = \frac{\partial}{\partial t}\left( \iiint_{CV} u\rho dV\right) + \iint_{CS} u\rho \left( \vec{v}.d\vec{S}\right) \]
<p>The last term is the energy flowing into or out of the control volume as carried by the fluid particles. For instance, in the figure above where I&#39;m talking about the particle needing to displace existing particles to make space for itself, that new particle might also be <em>hotter</em> than the existing particles. So, it will effectively add energy into the pipe. This is the rightmost term above. </p>
<p>So, if our control volume was just a pipe, this energy could only come in or out from the ports. Then, our complicated looking integral would simplify to \(\iint_{CS} u\rho \left( \vec{v}.d\vec{S}\right)=\Sigma{m.h}_{in}-\Sigma{m.h}_{out}\).</p>
<p>Well done&#33; You now know about as much as I do about how the three governing equations of fluid flow are set up. In future articles, we will be able to start formulating these equations in a format that the computer can understand. See you then&#33;</p>
<table class="fndef" id="fndef:1">
    <tr>
        <td class="fndef-backref"><a href="#fnref:1">[1]</a></td>
        <td class="fndef-content">I blame Kays and Crawford - Kays, W. M., and M. E. Crawford. &quot;Convective Heat and Mass Transfer&quot;, McGraw-Hill, New York, 1980.</td>
    </tr>
</table>

</div><!-- CONTENT ENDS HERE -->
    
        <script src="/libs/katex/katex.min.js"></script>
<script src="/libs/katex/auto-render.min.js"></script>
<script>renderMathInElement(document.body)</script>

    
    
  </body>
</html>
