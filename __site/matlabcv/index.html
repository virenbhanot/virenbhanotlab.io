<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/basic.css">
<link rel="stylesheet" href="/css/franklin.css">
<link rel="stylesheet" href="/libs/katex/katex.min.css">
<link rel="stylesheet" href="/libs/highlight/github.min.css">
<title>Modelling a Control Volume in Matlab</title> 
</head>
<body>
<header>
  <div class="blog-name"><a href="/">Isolated System</a></div>
</header><div class="franklin-content"><h1 id="modelling_a_control_volume_in_matlab"><a href="#modelling_a_control_volume_in_matlab" class="header-anchor">Modelling a control volume in Matlab</a></h1>
<p>Let&#39;s discuss how to develop a Minimum Viable Model of a control volume in Matlab. A &quot;control volume&quot; is nothing more than a region of space through which we want to study the flow of a fluid. This is a good exercise because it covers several areas of thermofluids: the upwind scheme, ODEs, calculations of partial derivatives and so on.</p>
<p>Our control volume will represent a pipe: rigid &#40;volume doesn&#39;t change&#41; and has two ports &#40;inlet and outlet&#41;. We will assume that the wall is perfectly adiabatic, that is, no heat exchange occurs between the pipe and the surroundings bar at the inlet and outlet ports.</p>
<p>Note that if you want to follow along, you will need to have a copy of Matlab &#40;or maybe Octave will do&#41; and the NIST software Refprop &#40;REFrigerant PROPerties&#41;.</p>
<h2 id="preliminaries"><a href="#preliminaries" class="header-anchor">Preliminaries</a></h2>
<p>Ok, so where do we begin? Well to fully know a control volume, we need to know three things about it: two independent thermodynamic variables and a flow variable. The thermodynamic variables let us calculate all of the properties like density, entropy, vapour quality etc. The flow variable lets us calculate the momentum of the fluid.</p>
<p>In the field of thermofluids, we often use Pressure and Specific Enthalpy as the state variables. We rely on the pressure-specific enthalpy diagram often to try to make sense of thermodynamic cycles, so it&#39;s a natural choice. Also, in the two-phase region &#40;the &#39;dome&#39; in the P-h diagram below&#41; knowing the enthalpy tells us <em>how far along</em> we are into the two-phase, aka, how much of the fluid we have evaporated/condenesed. This is needed for us in detector cooling applications to know the vapour quality.</p>
<p><img src="/assets/co2ph.jpg" alt="" /></p>
<p>Mass flow rate &#40;kg/s&#41; is selected as the flow variable rather than the volumetric flow rate &#40;L/s&#41; or velocity &#40;m/s&#41; because it is more &#39;objective&#39;. It is unaffected by changes in cross sectional areas. For steady flow, the mass flow rate will be the same everywhere, in a way that the volumetric flow rate and velocity won&#39;t be.</p>
<p>We won&#39;t do any discretisation of the pipe that we&#39;re modelling. In reality, you could subdivide the pipe into smaller segments for more accurate values, but we&#39;ll just think of it as one big lump.</p>
<p>So, we have a &#39;lumped&#39; volume, and we need to know three things about it: the pressure, the specific enthalpy and the mass flow rate.</p>
<h2 id="upwind_scheme"><a href="#upwind_scheme" class="header-anchor">Upwind scheme</a></h2>
<p>Let&#39;s talk about one of my most favourite mathematical techniques ever. We use the upwind technique to be able to cohesively determine <em>what part of the model calculates what</em>. For the current model, we need the technique to determine what are the boundary conditions, vs what is calculated by the model itself.</p>
<p>We <em>could</em>, if we wanted, solve all three equations inside the same control volume. The trouble is this: the three governing equations all are deeply interconnected. Changes in one equation will have impacts on the other equations. We call this having the equations &#39;coupled&#39;. This is a nightmare situation for the differential equation solvers because it brings them to a crawl.</p>
<p>To avoid this, Courant et al<sup id="fnref:2"><a href="#fndef:2" class="fnref">[1]</a></sup>, back in 1952, proposed the upwind scheme, which was later covered by Patankar<sup id="fnref:3"><a href="#fndef:3" class="fnref">[2]</a></sup> in his brilliant 1980 book. Using this method, we &#39;decouple&#39; the equations from each other. We solve the mass and energy balance on one grid called the &#39;thermal grid&#39;, and, offset by half a cell width, we solve the momentum balance on the &#39;momentum grid&#39;.</p>
<p><img src="/assets/matlabcv_2.png" alt="" /></p>
<p>The cells are the same width, and thus, offsetting by half a cell-width means that the centers of one grid coincide with the boundaries of the other grid. This way, whatever is calculated inside one grid is automatically averaged right at the boundary of the neighbouring grid, and thus no interpolation is necessary.</p>
<p><img src="/assets/matlabcv_3.png" alt="" /></p>
<p>This concept is pretty tricky to understand, but ultimately, it&#39;s sufficient if you remember that, if we&#39;re dealing with a capacitive component, the mass flow rate is determined by the upwind scheme, and, if we&#39;re dealing with a resistive component, the pressure is determined by the upwind component. The incoming enthalpy, meanwhile, is ALWAYS determined by upstream conditions.</p>
<h2 id="boundary_and_initial_conditions"><a href="#boundary_and_initial_conditions" class="header-anchor">Boundary and Initial Conditions</a></h2>
<p>So, having discussed the upwind scheme, we now take the control volume itself to be a &#39;capacitive&#39; element. That is, the control volume solves for the pressure and specific enthalpy using the mass and energy balance equations. Then, the inlet and outlet ports must supply the &#39;resistive&#39; element of the equation, aka the mass flow rate. Then, just before the inlet port would be some momentum grid element, and likewise for just beyond the outlet port, were we to build up a complete system model.</p>
<p>Note, however, that in addition to the mass flow rate, we also need to supply the enthalpy of the incoming fluid, as the upwind scheme dictates that the inlet enthalpy be defined by the component upstream of it. Thus, the system setup ends up looking like:</p>
<p><img src="/assets/matlabcv_1.png" alt="" /></p>
<p>In addition, when solving differential equations that involve time, we need to provide initial conditions. We need as many initial conditions as there are dynamic variables, two in this case &#40;pressure and enthalpy&#41;.</p>
<h2 id="the_equations"><a href="#the_equations" class="header-anchor">The equations</a></h2>
<p>Now, let&#39;s talk about the governing equations. First, the mass balance.</p>
<h3 id="mass_balance"><a href="#mass_balance" class="header-anchor">Mass Balance</a></h3>
<p>Our flow is one-dimensional. Thus, our equation will take the form:</p>
\[\frac{\partial{(\rho V)}}{\partial t} = \dot{m}_{in}-\dot{m}_{out}\]
<p>Rather straightforward, right? The equation basically states that any refrigerant flowing into the control volume can do one of two things: 1&#41; increase the mass inside the volume, or 2&#41; flow out of the volume.</p>
<p>Notice, however, that our volume is not deformable. The control volume is rigid, and thus the derivative of Volume with time will be zero. The equation then becomes:</p>
\[V\frac{\partial{(\rho)}}{\partial t} = \dot{m}_{in}-\dot{m}_{out}\]
<p>Next, we notice that the equation above is in terms of density, and not in terms of pressure and specific enthalpy, which are our state variables. So what do we do?</p>
<p>We first remember that in thermodynamics, any variable can be found using two other known variables. Then, \(\rho=f(P,h)\)</p>
<p>If we apply the chain rule of differentiation to this fact, we get</p>
\[V\frac{\partial{\rho}}{\partial t} = \frac{\partial\rho}{\partial P}\frac{dP}{dt}\Bigg|_h + \frac{\partial\rho}{\partial h}\frac{dh}{dt}\Bigg|_P\]
<p>Here, the pipe symbols with the subscripts mean that when we are taking the partial derivative of the pressure, we keep enthalpy constant &#40;and vice versa&#41;.</p>
<p>Substituting this into the mass balance equation gives:</p>
\[V\left(\frac{\partial\rho}{\partial P}\frac{dP}{dt}\Bigg|_h + \frac{\partial\rho}{\partial h}\frac{dh}{dt}\Bigg|_P\right) = \dot{m}_{in}-\dot{m}_{out}\]
<p>These partial derivatives will be calculated using Refprop and some fluid property magic in the two-phase region. But now let&#39;s do the same for the energy balance.</p>
<h3 id="energy_balance"><a href="#energy_balance" class="header-anchor">Energy Balance</a></h3>
<p>The energy balance equation will look like this:</p>
\[\frac{\partial{(\rho uV)}}{\partial t} = \dot{m}h_{in}-\dot{m}h_{out}\]
<p>This equation is along the same lines as the previous ones, stating that if we have a flow coming in &#40;carrying energy&#41;, it can either flow out, or raise the internal energy of the fluid inside. </p>
<p>Now, as before, we note that the volume is constant. So, we are left with \(\rho.u\) inside the derivative. Then, we also know that the enthalpy is \(h = u + P/\rho\). Thus, \(u = h - P/\rho\). If we substitute this into the equation above and run the crank of deriving it out, we will get:</p>
\[V\left(\left(h\frac{\partial\rho}{\partial P}-1\right)\frac{dP}{dt}\Bigg|_h + \left(h\frac{\partial\rho}{\partial h}+\rho\right)\frac{dh}{dt}\Bigg|_P\right) = \dot{m}h_{in}-\dot{m}h_{out}\]
<h2 id="implementing_the_code_in_matlab"><a href="#implementing_the_code_in_matlab" class="header-anchor">Implementing the code in Matlab</a></h2>
<p>Right&#33; Onto the good stuff. How do we implement this in matlab? Let&#39;s first try to understand how ODEs are set up in Matlab. Well, ODEs are set up in the form of matrix equations in Matlab. They take the form <code>Ax &#61; B</code>, with x being the vector of state variables, A is the matrix of the partial derivatives, while B contains the constraints, or the boundary conditions in our example.</p>
\[
\left[ \begin{matrix} V\frac{\partial\rho}{\partial P} & V\frac{\partial\rho}{\partial P} \\
V\left( h\frac{\partial\rho}{\partial P} - 1\right) & V\left(h\frac{\partial\rho}{\partial P} +\rho \right) \end{matrix} \right] \left[\begin{matrix}{\partial P}/{\partial t} \\ {\partial h}/{\partial t}\end{matrix}\right] = \left[ \frac{\dot{m}_{in}-\dot{m}_{out}}{\dot{m}h_{in}-\dot{m}h_{out}}\right]
\]
<p>Then, if we are able to set up a function that returns the values of these partial derivatives, we can send this to our ODE function, and it can integrate the equation over time. Thus, we need a function that outputs <code>x &#61; A\B</code></p>
<p>Let&#39;s look at that code now.</p>
<pre><code class="language-julia">function y &#61; controlVolume&#40;~,x,fld,Vol,mDotIn,hin,mDotOut&#41;
&#37; controlVolume Lumped control volume
&#37; Inlet boundary conditions: mass flow rate and enthalpy
&#37; Outlet boundary condns: mDot
&#37; State variables: Pressure &#40;Pa&#41; and enthalpy &#40;J/kg&#41;

P &#61; x&#40;1&#41;; &#37; Pa
h &#61; x&#40;2&#41;; &#37; J/kg

&#37; Function I wrote to calculate two-phase partial derivatives:
&#91;drho_dPh,drho_dhP&#93; &#61; getPartialDers&#40;P,h,fld&#41;; 

rho &#61; refpropm&#40;&#39;D&#39;,&#39;P&#39;,P*1e-3,&#39;H&#39;,h,fld&#41;;

Hin &#61; mDotIn*hin;
Hout &#61; mDotOut*h;

a &#61; Vol*drho_dPh;
b &#61; Vol*drho_dhP;    
c &#61; Vol*&#40;h*drho_dPh-1&#41;;
d &#61; Vol*&#40;h*drho_dhP&#43;rho&#41;;

M &#61; &#91;a,b ; c,d&#93;;
f &#61; &#91;mDotIn-mDotOut ; Hin-Hout&#93;;

y &#61; M\f;
end</code></pre>
<p>This code should hopefully look pretty understandable to you by now. The matrices A and B are here called M and f to be more consistent with the implicit equation nomenclature that Matlab uses. Don&#39;t worry about it. It&#39;s just another name.</p>
<p>So, what is this function spitting out? The values of dy/dt, aka &#91;dP/dt ; dh/dt&#93;. The <code>getPartialDerivatives</code> function calculates the partial derivatives, mindful of where we are in the Pressure-enthalpy diagram &#40;single phase or two-phase&#41;.</p>
<pre><code class="language-julia">&#37; LUMPED CONTROL VOLUME - P/h STATE VARIABLES
&#37; Pressure &#40;Pa&#41; and specific enthalpy &#40;J/kg&#41; state variables
&#37; Inlet and outlet mass flow rates and enthalpies boundary conditions
&#37; &#40;Note: Pressure in Pa for state var, bar for plots and kPa for refprop&#41;

clc
clear
close all

fld &#61; &#39;CO2&#39;;

&#37; Time discretization
t &#61; 60; &#37; Simulation time &#91;s&#93;
dt &#61; 1; &#37; &#91;s&#93;
tspan &#61; 0:dt:t;
nt &#61; length&#40;tspan&#41;;

&#37; Geometry
D &#61; 0.2; &#37; &#91;m&#93;
L &#61; 0.32; &#37; &#91;m&#93;
Vol &#61; pi * &#40;D/2&#41;^2 * L; &#37; &#91;m3&#93;

&#37; Boundary Conditions
mDotIn &#61; 0.01; &#37; &#91;kg/s&#93;
hin &#61; 100000; &#37; &#91;J/kg&#93;
mDotOut &#61; 0.01;

&#37; Initial Conditions
P0 &#61; 20e5; &#37; Pa
h0 &#61; 250000; &#37; J/kg
x0 &#61; &#91;P0,h0&#93;; &#37;

&#37; Solving ODEs
optionSet &#61; odeset&#40;&#39;OutputFcn&#39;,@odeplot,&#39;RelTol&#39;,1e-5&#41;;
&#91;t,x&#93; &#61; ode15s&#40;@&#40;t,x&#41; controlVolume&#40;t,x,fld,Vol,mDotIn,hin,mDotOut&#41;, tspan, x0, optionSet&#41;;

&#37; Plotting
set&#40;0,&#39;defaultlinelinewidth&#39;,1.5&#41;;
set&#40;0,&#39;DefaulttextFontSize&#39;,14&#41;;
set&#40;0,&#39;DefaultaxesFontSize&#39;,10&#41;;
P &#61; x&#40;:,1&#41;.*1e-5;
h &#61; x&#40;:,2&#41;.*1e-3;
plotPh&#40;&#41;; &#37; my own function for plotting the pressure-enthalpy diagram
plot&#40;h,P,&#39;r&#39;&#41;;
text&#40;h&#40;1&#41;,P&#40;1&#41;,&#39;\leftarrow Start&#39;&#41;;
text&#40;h&#40;end&#41;,P&#40;end&#41;,&#39;\leftarrow End&#39;&#41;;</code></pre>
<p>Right, so a couple of things to comment here. First the simulation parameters. The way I set up the simulation above is to have a 10 L vessel, full of a two-phase mixture of CO2. Then, we have 10 g/s flowing in, and 10 g/s flowing right out again. The incoming fluid is at very cold enthalpies of 100 kJ/kg &#40;corresponding to &lt; -45°C at 20 bar&#41;.</p>
<p>So, what would you expect? Well, since the <em>flow rate</em> in and flow out is the same, the mass should stay constant. And indeed, that is what we see. Look at the P-h diagram below</p>
<p><img src="/assets/matlabcv_5.svg" alt="" /></p>
<p>The green lines are the isochores, aka the constant density lines. The red line, as you can see, proceeds along the constant density line. Slowly, the control volume is also cooling down. All seems to make sense.</p>
<p>So, what have we achieved? Well, if you were able to follow along, you should now be able to create what is the basic component for all thermofluid simulations. By creating successions of such control volumes, we can model very complex systems indeed, and that is something I hope to continue talking about in future posts.</p>
<h2 id="references"><a href="#references" class="header-anchor">References</a></h2>
<p><table class="fndef" id="fndef:2">
    <tr>
        <td class="fndef-backref"><a href="#fnref:2">[1]</a></td>
        <td class="fndef-content">Courant, R., Isaacson, E., &amp; Rees, M. &#40;1952&#41;. On the solution of nonlinear hyperbolic differential equations by finite differences. Communications in Applied Numerical Methods, 5&#40;3&#41;, 243–255. http://doi.org/10.1002/cpa.3160050303 </td>
    </tr>
</table>
<table class="fndef" id="fndef:3">
    <tr>
        <td class="fndef-backref"><a href="#fnref:3">[2]</a></td>
        <td class="fndef-content">Patankar, S. &#40;1980&#41;. Numerical heat transfer and fluid flow. Series in Coputational Methods in Mechanics and Thermal Sciences. http://doi.org/10.1016/j.watres.2009.11.010 </td>
    </tr>
</table>
</p>
</div><!-- CONTENT ENDS HERE -->
    
        <script src="/libs/katex/katex.min.js"></script>
<script src="/libs/katex/auto-render.min.js"></script>
<script>renderMathInElement(document.body)</script>

    
    
        <script src="/libs/highlight/highlight.pack.js"></script>
<script>hljs.initHighlightingOnLoad();hljs.configure({tabReplace: '    '});</script>

    
  </body>
</html>
