<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/basic.css">
<link rel="stylesheet" href="/css/franklin.css">
<link rel="stylesheet" href="/libs/katex/katex.min.css">
<link rel="stylesheet" href="/libs/highlight/github.min.css">
<title>The Problem of Low Mass Flow Rate</title> 
</head>
<body>
<header>
  <div class="blog-name"><a href="/">Isolated System</a></div>
</header><div class="franklin-content"><h1 id="the_problem_of_low_mass_flow_rates"><a href="#the_problem_of_low_mass_flow_rates" class="header-anchor">The problem of low mass flow rates</a></h1>
<p>I first came across this issue in a vague reference in one of the seminal papers on object oriented modelling of thermofluid systems <sup id="fnref:1"><a href="#fndef:1" class="fnref">[1]</a></sup>. The idea is this: in turbulent flows, the pressure drop and the mass flow rate are related by a square relation, that is, the pressure drop is a function of the square of the flow. &#40;In laminar regions, that relation might be approximated as linear&#41;.</p>
<p>Now, before we delve any further in the discussion above, I should mention a few facts. First, as you know if you&#39;ve read elsewhere on this site, fluid flow in pipes is dictated by three equations: the mass, momentum and energy conservation equations. There is, however, a &#39;last-amongst-equals&#39; in these equations, for the kind of applications that I deal with. In particular, the momentum equation can be argued to be the least important of the three. The reason for that is that we are dealing with &#39;dynamics&#39; of things. Thus, almost by definition, things that change faster have less bearing <em>on the overall system response</em> than things that change slower.</p>
<p>Note that I&#39;m not saying that faster dynamics are inessential. I&#39;m saying that if we approximate their initial and final states, that&#39;s good enough because the actual paths that those changes take are obscured by the fact that something else is taking longer to change. In this case, those &quot;something elses&quot; are all of the thermal dynamics &#40;heat transfer, temperature change etc.&#41;. The reason the pressure transients are so much faster is that pressure waves &#40;aka &#39;changes&#39; in pressure&#41; travel through a medium at the speed of sound. This speed is usually WAY faster than the speed at which the fluid itself flows.</p>
<p>Thus, the punchline is that the pressure transients aren&#39;t that critical to be accurately modelled, and certainly not enough to implement loads of non-linear, complex equations into the model. Instead, we take simple forms of the equation. In particular, we specify the pressure drop \(dP_0\) at a particular, known, design mass flow rate \(\dot{m}_0\) and then calculate it everywhere else using the relation: \(\dot{m}=\dot{m}_0\sqrt{\frac{dP}{dP_0}}\).</p>
<p>Couple of things to note about that equation. The first is that we use the <em>pressure drop</em> to calculate the mass flow rate instead of the other way around. As I&#39;ve mentioned elsewhere, we need to calculate three things for each node: the pressure, the specific enthalpy and the mass flow rate. The second thing is that in the equation above, we make sure the sign of \(dP\) is positive and then account for flow direction elsewhere. The plot for this equation looks like this:</p>
<p><img src="/assets/lowmdot_1.svg" alt="" /></p>
<p>Here, I take \(\dot{m}_0\) &#61; 0.1 kg/s and \(dP_0\) &#61; 0.01 bar. I&#39;ve marked the point &#40;0.01, 0.1&#41; with a star on the plot too.</p>
<p>So, this equation above is a pretty easy and uncomplicated way of simplifying the momentum equation without getting into the weeds of calculating Reynold&#39;s numbers and friction factors. Great, right?</p>
<p>Not quite. Let&#39;s zoom in a little bit, in the region of mass flow rates between say, -0.1 kg/s to 0.1 kg/s:</p>
<p><img src="/assets/lowmdot_2.svg" alt="" /></p>
<p>I hope this gives you some hints about the issue. The problem is, when your mass flow is hovering around zero, as might be the case if you&#39;re simulating a valve closure or system shutdown, the flow rate changes <em>hugely</em> with even small changes for pressure drop. In fact, at exactly 0 mass flow rate, the slope of the plot is infinite&#33; Even worse,  there can be reversals in flow directions, going from positive flow one second to negative flow the next &#40;aka reverse flow&#41;. </p>
<p>These are big headaches for the solver and can bring it to its knees. The solver is compelled to take very small steps, because as soon as it attempts to take big steps, the oscillations in the numbers are through the roof.</p>
<p>This is no good.</p>
<p>To solve this, we smoothen the plot in the small mass flow rate region. We perform something called piecewise monotonic cubic interpolation<sup id="fnref:2"><a href="#fndef:2" class="fnref">[2]</a></sup>. Piecewise refers to the idea of dividing the full region into sub-regions. A monotonic function \(y = f(x)\) means that, given two points \(x_1\) and \(x_2\), if \(x_2 > x_1\) then, <em>you are guaranteed that \(y_2 > y_1\)</em>. Thus, the curve, in going left to right, never &#39;dips&#39;. Cubic means the polynomial has a cube in it, and interpolation means finding missing points between two &#40;or more&#41; given points.</p>
<p>By using the monotonicity in our interpolation, we are making sure that the solver doesn&#39;t come across the puzzling scenario where, although the pressure drop increased, the flow actually decreased.</p>
<p>The &#39;small&#39; flow region is, of course, a relative term. It depends on the application being studied. Anyway, the function that Modelica uses is called <code>regRoot2</code> and I&#39;ve made my own implementations of it in EcosimPro and Matlab. The Modelica code can be found <a href="https://www.maplesoft.com/documentation_center/online_manuals/modelica/Modelica_Fluid_Utilities.html#Modelica.Fluid.Utilities.regRoot2">here</a>. The Matlab code is at the end of this article. At any rate. Let&#39;s take our original graph and add the &#39;regularised&#39; &#40;aka smoothened&#41; plot to compare.</p>
<p><img src="/assets/lowmdot_3.jpg" alt="" /></p>
<p>See what I mean? Now, the going is much smoother in the small mass flow regions. The slope at zero flow rate is finite, and virtually linear &#40;which I alleged is the case in laminar, aka low-flow regions&#41;.</p>
<p>Coming across this trick was a big lifesaver for me, and has made much of my simulations smoother, more robust, and faster. So thanks Dr. Elmqvist et al&#33;</p>
<p>My copycat implementation of regRoot2 in Matlab is below. Note that it is divided across three functions:</p>
<pre><code class="language-julia">function &#91;y&#93; &#61; regRoot2&#40;x,varargin&#41;
&#37;REGROOT2 Anti-symmetric approximation of square root with discontinuous
&#37;factor so that the first derivative is finite and continuous
&#37;   INPUTS:
&#37;   x: absicca value
&#37;   x_small: threshold for small x approximation
&#37;   k1: if x&gt;&#61;0, y &#61; sqrt&#40;k1*x&#41; &#40;minimum &#61; 0&#41;
&#37;   k2: if x&lt;&#61;0, y &#61;-sqrt&#40;k2*x&#41; &#40;minimum &#61; 0&#41;
&#37;   yd0: desired derivative at x&#61;0 &#40;default &#61; 1&#41;
if nargin&#61;&#61;1
    x_small &#61; 0.01;
    k1 &#61; 1;
    k2 &#61; 1;
    use_yd0 &#61; 0;
    yd0 &#61; 1;
elseif nargin&#61;&#61;2
    x_small &#61; varargin&#123;1&#125;;
    k1 &#61; 1;
    k2 &#61; 1;
    use_yd0 &#61; 0;
    yd0 &#61; 1;
elseif nargin&#61;&#61;3
    x_small &#61; varargin&#123;1&#125;;
    k1 &#61; varargin&#123;2&#125;;
    k2 &#61; 1;
    use_yd0 &#61; 0;
    yd0 &#61; 1;
elseif nargin&#61;&#61;4
    x_small &#61; varargin&#123;1&#125;;
    k1 &#61; varargin&#123;2&#125;;
    k2 &#61; varargin&#123;3&#125;;
    use_yd0 &#61; 0;
    yd0 &#61; 1;
elseif nargin&#61;&#61;5
    x_small &#61; varargin&#123;1&#125;;
    k1 &#61; varargin&#123;2&#125;;
    k2 &#61; varargin&#123;3&#125;;
    use_yd0 &#61; 1;
    yd0 &#61; varargin&#123;4&#125;;
else
    error&#40;&#39;Too many input arguments&#33;&#39;&#41;;
end

if x&gt;&#61;x_small
    y &#61; sqrt&#40;k1*x&#41;;
elseif x&lt;&#61;-x_small
    y &#61; -sqrt&#40;k2*abs&#40;x&#41;&#41;;
elseif k1&gt;&#61;k2
    y &#61; regRoot2_utility&#40;x,x_small,k1,k2,use_yd0,yd0&#41;;
else
    y &#61;-regRoot2_utility&#40;-x,x_small,k2,k1,use_yd0,yd0&#41;;
end
end


function &#91;y&#93; &#61; regRoot2_utility&#40;x,x1,k1,k2,use_yd0,yd0&#41;
&#37;UNTITLED3 Summary of this function goes here
&#37;   Detailed explanation goes here
x2 &#61; -x1*&#40;k2/k1&#41;;
if x&lt;&#61;x2
    y &#61; -sqrt&#40;k2*abs&#40;x&#41;&#41;;
else
    y1 &#61; sqrt&#40;k1*x1&#41;;
    y2 &#61; -sqrt&#40;k2*abs&#40;x2&#41;&#41;;
    y1d &#61; sqrt&#40;k1/x1&#41;/2;
    y2d &#61; sqrt&#40;2/abs&#40;x2&#41;&#41;/2;
    if use_yd0&#61;&#61;1
        y0d &#61; yd0;
    else
        w &#61; x2/x1;
        y0d &#61; &#40;&#40;3*y2-x2*y2d&#41;/w - &#40;3*y1-x1*y1d&#41;*w&#41;/&#40;2*x1*&#40;1-w&#41;&#41;;
    end
    w1 &#61; sqrt&#40;8.75*k1/x1&#41;;
    w2 &#61; sqrt&#40;8.75*k2/abs&#40;x2&#41;&#41;;
    y0d &#61; min&#40;y0d,0.9*min&#40;w1,w2&#41;&#41;;
    if x&gt;&#61;0
        y &#61; y1*evaluatePoly3_derivativeAtZero&#40;x/x1,1,1,y1d*x1/y1,y0d*x1/y1&#41;;
    else
        y &#61; y1*evaluatePoly3_derivativeAtZero&#40;x/x1,x2/x1,y2/y1,y2d*x1/y1,y0d*x1/y1&#41;;
    end
end
end


function &#91;y&#93; &#61; evaluatePoly3_derivativeAtZero&#40;x,x1,y1,y1d,y0d&#41;
&#37;evaluatePoly3_derivativeAtZero Evaluate polynomial of order 3 that passes
&#37;at origin with a predefined derivative
&#37;   Inputs:
&#37;   x: value for which polynomial evaluated
&#37;   x1: abscissa value
&#37;   y1: y1 &#61; f&#40;x1&#41;
&#37;   y1d: first derivative at y1
&#37;   y0d: first derivative at f&#40;x&#61;0&#41;
a1 &#61; x1*y0d;
a2 &#61; 3*y1-x1*y1d-2*a1;
a3 &#61; y1-a2-a1;
xx &#61; x/x1;
y &#61; xx*&#40;a1&#43;xx*&#40;a2&#43;xx*a3&#41;&#41;;
end</code></pre>
<p><table class="fndef" id="fndef:1">
    <tr>
        <td class="fndef-backref"><a href="#fnref:1">[1]</a></td>
        <td class="fndef-content">Elmqvist, Hilding &amp; Tummescheit, Hubertus &amp; Otter, Martin. &#40;2003&#41;. Object-Oriented Modeling of Thermo-Fluid Systems. 269-286. </td>
    </tr>
</table>
<table class="fndef" id="fndef:2">
    <tr>
        <td class="fndef-backref"><a href="#fnref:2">[2]</a></td>
        <td class="fndef-content">Fritsch F.N. and Carlson R.E. &#40;1980&#41;, Monotone piecewise cubic interpolation. SIAM J. Numerc. Anal., Vol. 17, No. 2, April 1980, pp. 238-246</td>
    </tr>
</table>
</p>
</div><!-- CONTENT ENDS HERE -->
    
        <script src="/libs/katex/katex.min.js"></script>
<script src="/libs/katex/auto-render.min.js"></script>
<script>renderMathInElement(document.body)</script>

    
    
        <script src="/libs/highlight/highlight.pack.js"></script>
<script>hljs.initHighlightingOnLoad();hljs.configure({tabReplace: '    '});</script>

    
  </body>
</html>
